import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { RouterOptions } from 'express';
import { BlogStateResolver } from './core/resolvers/blog-state.resolver';
import { PageComponent } from './page/page.component';
import { AuthPageComponent } from './shared/templates/pages/auth-page/auth-page.component';
import { SearchPageComponent } from './shared/templates/pages/search-page/search-page.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch:  'full',
    resolve: [BlogStateResolver]
  },
  {
    path:'log-in',
    component: AuthPageComponent,
    resolve: [BlogStateResolver]
  },
  {
    path:'sign-up',
    component: AuthPageComponent,
    resolve: [BlogStateResolver]
  },
  // {
  //   path:'search',
  //   component: SearchPageComponent,
  //   resolve: [BlogStateResolver]
  // },
  {
    path:':slug',
    component: PageComponent,
    resolve: [BlogStateResolver]
  },

  {
    path:':type/:slug',
    component: PageComponent,
    resolve: [BlogStateResolver]
  },
];

const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  relativeLinkResolution: 'legacy',
  initialNavigation: 'enabled',
  // enableTracing: true
}

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
