import { Component } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { BlogPage } from './core/interfaces/page.interface';
import { LayoutService } from './core/services/layout.service';
import { FetchNavItems } from './core/store/actions/navigation.actions';
import { AppState } from './core/store/state/app.store';
import { JWTWatcher } from './core/watchers/jwt.watcher';
import { FetchAllAdBanner } from './store/actions/ad-banner.actions';
import { FetchAllCategories } from './store/actions/categories.actions';
import { FetchAllPages } from './store/actions/page.actions';
import { FetchAllTags } from './store/actions/tags.actions';
import { TemplateEngineState } from './store/state/template-engine.store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog';
  @Select(TemplateEngineState.getSelectedPage) selectedPage$: Observable<BlogPage>
  @Select(AppState.activeBreakpoint) breakPoint$: Observable<string>;

  constructor(private layoutService: LayoutService, private store: Store, private tokenWatcher: JWTWatcher){
    this.store.dispatch([
      new FetchAllAdBanner()
    ]);
    this.tokenWatcher;
    this.layoutService.subscribeToLayoutChanges().subscribe();
  }
}
