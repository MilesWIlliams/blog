import { BrowserModule, } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorHandler, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogRouterStateSerializer } from './core/serializers/router.serializer';
import { NgxsRouterPluginModule, RouterStateSerializer } from '@ngxs/router-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { CoreModule } from './core/core.module';

import { PageComponent } from './page/page.component';
import { NgxsModule } from '@ngxs/store';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { SharedModule } from './shared/shared.module';
import { states } from './store/state';
import { environment } from 'src/environments/environment';
import { MyErrorHandler } from './core/middleware/error-handler.middleware';

@NgModule({
  declarations: [
    AppComponent,
    PageComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    AppRoutingModule,
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsRouterPluginModule.forRoot(),
    CoreModule,
    SharedModule,
    NgxsModule.forRoot([...states], { developmentMode: !environment.production }),
    NgxsStoragePluginModule.forRoot({
      key: 'app'
    })
  ],
  providers: [
    { provide: RouterStateSerializer, useClass: BlogRouterStateSerializer },
    {provide: ErrorHandler, useClass: MyErrorHandler}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
