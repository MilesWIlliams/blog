import { InjectionToken } from "@angular/core";
import { AppConfig } from "../interfaces/app-config.interface";

export const Config:AppConfig = {
    gtm_id: '',
    secret_key: '',
    api_base: 'http://blog:8888/graphql',
};

export const VIEWER = new InjectionToken<string>('viewer');
export const CRAWLER_AGENTS = [
    'googlebot', 'yandexbot', 'yahoo', 'bingbot',
    'baiduspider', 'facebookexternalhit', 'twitterbot', 'rogerbot',
    'linkedinbot', 'embedly', 'quora link preview', 'showyoubot', 'outbrain',
    'pinterest/0.', 'developers.google.com/+/web/snippet',
    'slackbot', 'vkshare', 'w3c_validator', 'redditbot', 'applebot',
    'whatsapp', 'flipboard', 'tumblr', 'bitlybot', 'skypeuripreview',
    'nuzzel', 'discordbot', 'google page speed'
];