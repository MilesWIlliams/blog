import { Component, OnDestroy, OnInit } from '@angular/core';
import { RouterNavigation } from '@ngxs/router-plugin';
import { Actions, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AssignPage } from 'src/app/store/actions/page.actions';
import { FetchAllPosts, ViewSinglePost } from 'src/app/store/actions/posts.actions';
import { SearchPosts } from 'src/app/store/actions/search.actions';
import { PostState } from 'src/app/store/state/post.store';

import { PageTypeEnum } from '../../enums/page-type.enum';
import { AppRoute } from '../../interfaces/app-route.interface';
import { FetchPageConfig } from '../../interfaces/page.interface';
import { Post } from '../../interfaces/post.interface';
import { SetSelectedNavItem, UpdateURL } from '../../store/actions/navigation.actions';
import { AppState } from '../../store/state/app.store';
import { NavigationState } from '../../store/state/navigation.store';
import { PageType } from '../../types/page.types';

@Component({
  selector: 'blog-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {
  @Select(NavigationState.getMenuItems('PRIMARY')) menuItems$: Observable<AppRoute[]>;
  @Select(NavigationState.getSelectedNavID) selectedMenuItem$: Observable<string>;
  @Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;

  private selectedPost: Post;
  private destroy$ = new Subject<void>();
  public currentUrl: string = '';

  constructor(private store: Store, private actions$: Actions) {

    actions$.pipe(
      ofActionSuccessful(RouterNavigation),
      takeUntil(this.destroy$)
    ).subscribe((action: RouterNavigation) => {

      const snapshot = action.routerState as any;
      this.currentUrl = action.routerState.url;

      let path: string = snapshot.url;

      if (snapshot.params.type) {
        if (snapshot.params.type === PageTypeEnum.Category) {
          path = `/${snapshot.params.type}/${snapshot.params.slug}`;
        }
        else {
          path = snapshot.params.type;
        }
      } else {
        path = snapshot.params.slug || snapshot.url;
      }

      let pageType: PageType = 'page';

      if (snapshot.params && !snapshot.params.type) {

        if(snapshot.params.postId) pageType = PageTypeEnum.Post
      }
      const pageConfig: FetchPageConfig = {
        type: snapshot.params.type ? snapshot.params.type  : pageType,
        slug: ''
      }

      if (snapshot.params && snapshot.params.slug ) {
        pageConfig.slug = snapshot.params.slug;
      } else {

        pageConfig.slug = path.split('/')[1]
      }


      if (!path.includes('/')) {
        path = `/${path}`;
      }
      path = `${path}/`;

      const actions: any[] = [new UpdateURL(snapshot.url),new AssignPage(pageConfig), new SetSelectedNavItem(path)];

      if (pageType === PageTypeEnum.Post ) {
        this.selectedPost = this.store.selectSnapshot(PostState.getSelectedPost);
        if (!this.selectedPost) actions.push(new ViewSinglePost(snapshot.params.slug))
      } else if (pageType === PageTypeEnum.Page && pageConfig.slug === 'posts') {
        actions.push(new FetchAllPosts())
      }


      if (snapshot.queryParams?.query) actions.push(new SearchPosts({query: snapshot.queryParams.query}))
      this.store.dispatch([...actions])
    });
  }

  ngOnInit(): void {

  }

  public isAuthPage() {
    return this.currentUrl.includes('log-in' || 'sign-up');
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}
