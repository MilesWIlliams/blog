import { NgModule, Optional, PLATFORM_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreComponentsModule } from './core-components/core-components.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { services } from './services';
import { resolvers } from './resolvers';
import { guards } from './guards';
import { AppState } from './store/state/app.store';
import { NgxsModule } from '@ngxs/store';
import {LayoutModule} from '@angular/cdk/layout';
import { watchers } from './watchers';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import { VIEWER } from './config/app.config';
import { viewerFactory } from './factories/viewer.factory';
import { coreStates } from './store/state';


@NgModule({
  declarations: [],
  exports:  [CoreComponentsModule],
  providers:[
    ...guards,
    ...services,
    ...resolvers,
    ...watchers,
    {
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true,
		},
    {
      provide: VIEWER,
      useFactory: viewerFactory,
      deps: [PLATFORM_ID, [new Optional(), REQUEST]],
    }
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    CoreComponentsModule,
    NgxsModule.forFeature([...coreStates]),
    LayoutModule
  ]
})
export class CoreModule { }
