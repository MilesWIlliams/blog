export enum PageTypeEnum {
    Page = 'page',
    Category = 'category',
    Tag = 'tag',
    Post = 'posts',
    Review = 'reviews',
}