import { isPlatformBrowser } from "@angular/common";
import { CRAWLER_AGENTS } from "../config/app.config";

export function viewerFactory(platformId, req: Request): string {
    if (isPlatformBrowser(platformId)) {
        return 'user';
    }
    const userAgent = (req.headers['user-agent'] || '').toLowerCase();
    const isCrawler = CRAWLER_AGENTS.some(crawlerAgent => userAgent.indexOf(crawlerAgent) !== -1);
    return isCrawler ? 'bot' : 'user';
}