import { AuthGuard } from "./auth.guard";
import { NavGuard } from "./nav.guard";

export const guards = [ AuthGuard, NavGuard]