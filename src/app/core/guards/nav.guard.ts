import { Injectable } from '@angular/core';
import { CanActivate, CanLoad } from '@angular/router';
import { Navigate } from '@ngxs/router-plugin';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { switchMap, catchError, tap, filter, take } from 'rxjs/operators';
import { FetchNavItems } from '../store/actions/navigation.actions';

import { AppState } from '../store/state/app.store';
import { NavigationState } from '../store/state/navigation.store';

@Injectable({
	providedIn: 'root'
})
export class NavGuard implements CanLoad {
	constructor(private store: Store) { }

	canLoad(): Observable<boolean> {
		return this.checkStore().pipe(
			switchMap(() => of(true)),
			catchError(() => of(false))
		);
	}

	private checkStore(): Observable<boolean> {
		return this.store.select(NavigationState.navLoaded).pipe(
			tap(authed => {
				if (!authed) {
					this.store.dispatch(new FetchNavItems());
				}
			}),
			filter(loaded => loaded),
			take(1)
		);
	}
}