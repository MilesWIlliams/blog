import { Injectable } from '@angular/core';
import {
	HttpInterceptor,
	HttpRequest,
	HttpHandler,
	HttpEvent,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { AppState } from '../store/state/app.store';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private store: Store) {}
	intercept(
		req: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
        const token = this.store.selectSnapshot(AppState.Token);

        if (token) {
            req = req.clone({
                headers: req.headers.set(
                    'Authorization',
                    `Bearer ${token}`
                ),
            });
        }
		return next.handle(req);
	}
}