import { FeaturedImage } from "./media.interface";

export interface AdBanner {
    databaseId: number,
    excerpt: string,
    link: string,
    date: string,
    featuredImage: FeaturedImage
    title: string,
    bannerInfo: BannerInfo
}

export interface  BannerInfo {
    excerpt: string,
    fieldGroupName: string,
    title: string,
    postLink: {
        target: string,
        title: string,
        url: string,
    }
}