export interface AppRoute {
    id: string;
    path: string;
    isSelected:boolean;
    label: string;
    children?: ChildRoute[]
}

export interface AppMenu {
    databaseId: number;
    name: string;
    slug: string;
    locations: string[];
    menuItems?: AppRoute[];
}

export interface ChildRoute extends AppRoute {
    parentId: number;
}