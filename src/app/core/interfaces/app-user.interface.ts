import { Avatar } from "./media.interface";

export interface AppUser {
    id: string;
    avatar: Avatar;
    capabilities: string[];
    databaseId: number;
    email: string;
    username:string;
}

export interface RegisterUserPayload {
    email: string
    username: string;
    password: string;
}

export interface LoginUserPayload {
    username: string;
    password: string;
}