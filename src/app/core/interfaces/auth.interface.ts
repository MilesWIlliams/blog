import { AppUser } from "./app-user.interface";

export interface AuthLoginUser {
    username: string;
    password: string;
}
export interface AuthRegisterUser {
    email: string;
    username: string;
    password: string;
}

export interface AuthPaylod {
    auth_token: string;
    refresh_token: string;
    user: AppUser
}

export interface TokenRenewal {
    token: string;
}