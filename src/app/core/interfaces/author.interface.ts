import { Avatar } from "./media.interface";

export interface Author {
    databaseId: number,
    name: string,
    id: string,
    avatar: Avatar
}

export interface User  {
    id: string,
    avatar: Avatar,
    databaseId: number,
    name: string
}
