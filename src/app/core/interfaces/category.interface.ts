export interface Category {
    id: string,
    count: number,
    databaseId: number,
    description: string,
    isRestricted: boolean,
    name: string,
    slug: string,
}