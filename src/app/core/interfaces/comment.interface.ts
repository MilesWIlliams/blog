import { Author } from "./author.interface";

export interface PostComment {
    databaseId: number,
    date: string,
    id: string,
    approved: boolean,
    content: string,
    author: Author,
}

export interface PostCommentPayload {
    databaseId: number,
    author: string,
    authorEmail: string,
    content: string,
}