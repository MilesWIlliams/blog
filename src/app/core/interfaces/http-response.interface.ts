import HttpStatusCode from '../enums/HttpStatusCodes.enum';

/**
 *
 * @export
 * @interface HttpResponse
 * @description Generic api response object
 * @template T
 */
export interface HttpResponse<T> {
    message: string;
    values?: T;
    code: HttpStatusCode;
    query?: any;
}
