export interface Image {
    altText: string,
    caption: string,
    databaseId: number,
    date: string,
    mediaType: string,
    srcSet: string,
    sourceUrl: string,
    title: string,
}