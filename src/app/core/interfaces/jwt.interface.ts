export interface DecodedJWT {
	iss: string,
	iat: number,
	nbf: number,
	exp: number,
	data: {
		user: {
			id: string,
			user_secret: string
		}
	}
}