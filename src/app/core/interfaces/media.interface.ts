export interface FeaturedImage {
    altText?: string;
    link?: string;
    srcSet: string;
    sourceUrl?: string;
}

export interface Avatar {
    url: string;
}