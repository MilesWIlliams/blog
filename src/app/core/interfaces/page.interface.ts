import { PageType } from "../types/page.types";
import { FeaturedImage } from "./media.interface";
import { FrontPagePostGroupSection } from "./post.interface";

export interface BlogPage {
    id: string;
    content: any;
    databaseId: number;
    isFrontPage: boolean;
    slug: string;
    title: string;
    postsGroups?: FrontPagePostGroupSection;
    featuredImage: FeaturedImage;
    type?: PageType;
}

export interface FetchPageConfig {
    type: PageType,
    slug: string,
    id?: number
}