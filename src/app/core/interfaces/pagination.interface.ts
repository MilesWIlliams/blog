export interface Paginination {
    previous_post_url: string;
    next_post_url: string;
}