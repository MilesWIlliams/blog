import { Author } from "./author.interface";
import { Category } from "./category.interface";
import { PostComment } from "./comment.interface";
import { FeaturedImage } from "./media.interface";
import { Tag } from "./tag.interface";

export interface Post {
    id: string;
    authorId: string;
    author: Author;
    databaseId: number;
    date: string;
    categories: Category[];
    commentCount: number;
    comments?: PostComment[];
    content: string;
    cursor?: string;
    excerpt: string;
    featuredImageId: string;
    featuredImage: FeaturedImage;
    isPreview: boolean;
    isRestricted: boolean;
    next: {
        slug: string
    };
    previous: {
        slug: string
    };
    title: string;
    tags: Tag[];
    slug: string;
}

export interface FrontPagePostGroupSection {
    fieldGroupName: string,
    categoryPosts: {
        fieldGroupName: string,
        financesSection: FrontPagePostCategory,
        investmentSection: FrontPagePostCategory,
        propertySection: FrontPagePostCategory,
        taxSection: FrontPagePostCategory
    }
}

export interface FrontPagePostCategory {
    fieldGroupName: string,
    posts: Array<FrontPagePostCategorySection>,
    sectionTitle: string,
}

export interface FrontPagePostCategorySection {
    id: string,
    slug: string,
    title: string,
    content: string,
    excerpt: string,
    featuredImage: FeaturedImage,
    modified: string,
    commentCount: number,
    categories: {
        nodes: Array<{
            id: string,
            categoryId:  number,
            count: number,
            databaseId: number,
            description: string,
            isRestricted: boolean,
            name: string,
            slug: string
        }>
    }
}
