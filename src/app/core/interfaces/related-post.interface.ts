import { FeaturedImage } from "./media.interface";

export interface RelatedPost {
    id: string,
    title: string,
    slug: string,
    featuredImage: FeaturedImage
}