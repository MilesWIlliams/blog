import { AuthorQuery } from 'src/app/types/post.type';
import { Image } from './image.interface';

export interface CompanyDetailsDataObject {
    productPricingDetails: {
        feesOrPriceLabelText: string,
        price: string,
    },
    promotionsDetails: {
        promotionsLink: string,
        promotionsLinkText: string
    },
    startDate: string,
    websiteUrl: string
}
export interface Review {
    id: string,
    authorId: string,
    author: AuthorQuery,
    date: string,
    databaseId: number,
    excerpt: string,
    review: {
        rating: string,
        takeawaySummary: string,
        companyDetailsGroup: {
            affiliateDetails: {
                affiliateButtonText: string,
                affiliateLink: string
            },
            companysDetails: CompanyDetailsDataObject,
            logo: Image,
            prosAndCons: {
                cons: {
                    con1: string,
                    con2: string,
                    con3: string,
                    con4: string,
                    con5: string,
                },
                pros: {
                    pro1: string,
                    pro2: string,
                    pro3: string,
                    pro4: string,
                    pro5: string,
                }
            }
            type: string
        },
        fullReview: {
            theBad: {
                content: string,
                heading: string
            },
            theGood: {
                content: string,
                heading: string
            }
        },
        media: {
            image1: Image,
            image10: Image,
            image2: Image,
            image3: Image,
            image4: Image,
            image5: Image,
            image6: Image,
            image7: Image,
            image8: Image,
            image9: Image,
        }
    }
    title: string,
    status: string,
    slug: string,
}

export interface CompanyDetails {
    Website: string;
    StartDate: {
        label: string,
        date: string
    };
    Fees: string;
    Promotions: string;
}

