export interface Tag {
    id: string,
    count: number,
    description: string,
    isRestricted: boolean,
    name: string,
    slug: string,
    tagId: number
}