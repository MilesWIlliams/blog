import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Store } from '@ngxs/store';

import { CoreService } from '../services/core.service';
import { AppRoute } from '../interfaces/app-route.interface';
import { FetchNavItems, FetchNavItemsFail } from '../store/actions/navigation.actions';


@Injectable({ providedIn: 'root' })
export class AppRoutingResolver implements Resolve<AppRoute[]> {

    constructor(private service: CoreService, private store: Store) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {

        return this.service.getAllNavigationMenues().pipe(
            tap(
                (res) => this.store.dispatch(new FetchNavItems()),
                (err) => this.store.dispatch(new FetchNavItemsFail(err))
            )
        );
    }
}
