import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { FetchAllCategories } from 'src/app/store/actions/categories.actions';
import { FetchAllPages } from 'src/app/store/actions/page.actions';
import { FetchFeaturedPosts } from 'src/app/store/actions/posts.actions';
import { FetchAllTags } from 'src/app/store/actions/tags.actions';
import { TemplateEngineState } from 'src/app/store/state/template-engine.store';
import { FetchNavItems } from '../store/actions/navigation.actions';

@Injectable({
	providedIn: 'root'
})
export class BlogStateResolver implements Resolve<boolean> {
	constructor(private store: Store) { }

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
		return this.checkStore()
	}

	private checkStore(): Observable<boolean> {
        const hasLoaded = this.store.selectSnapshot(TemplateEngineState.isLoaded);

        if (hasLoaded) return of(true);
        return this.store.dispatch([
            new FetchNavItems(),
            new FetchAllPages(),
            new FetchFeaturedPosts(),
            new FetchAllCategories(),
            new FetchAllTags()
        ]);
	}
}