import { AppRoutingResolver } from './app-routing.resolver';
import { BlogStateResolver } from './blog-state.resolver';
import { PostResolver } from './post.resolver';

export const resolvers: any[] = [BlogStateResolver, PostResolver];
