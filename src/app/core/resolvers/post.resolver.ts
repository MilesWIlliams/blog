import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { ViewSinglePost } from 'src/app/store/actions/posts.actions';
import { PostState } from 'src/app/store/state/post.store';

@Injectable({
        providedIn: 'root'
})
export class PostResolver implements Resolve<boolean> {
        constructor(private store: Store, private route: ActivatedRoute) { }

        resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
                return this.checkStore()
        }

        private checkStore(): Observable<boolean> {
                const hasLoaded = this.store.selectSnapshot(PostState.getSelectedPost);

                if (hasLoaded !== undefined || hasLoaded !== null) return of(true);
                return this.store.dispatch([new ViewSinglePost(this.route.snapshot.params['slug'])]);
        }
}