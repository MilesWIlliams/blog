import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AdBannerQuery } from 'src/app/types/ad-banner';
import { GraphQLService } from './base-api.service';

@Injectable({
    providedIn: 'root'
})
export class AdBannerService extends GraphQLService {

    constructor(public _http: HttpClient) {
        super(_http);
    }

    public fetchBanners(): Observable<AdBannerQuery>  {

        const FETCHBANNERS = `{
            banners {
                edges {
                    node {
                        id
                        databaseId
                        excerpt
                        link
                        date
                        featuredImage {
                            node {
                                srcSet
                                link,
                                sourceUrl
                            }
                        }
                        title
                        bannerInfo {
                            excerpt
                            fieldGroupName
                            title
                            postLink {
                                target
                                title
                                url
                            }
                        }
                    }
                }
            }
        }`

        return this.query({
            query: FETCHBANNERS
        });
    }
}
