import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { LoginUserResponseQuery, RegisterUserResponseQuery } from 'src/app/types/users.type';
import { AuthLoginUser, AuthPaylod, AuthRegisterUser, TokenRenewal } from '../interfaces/auth.interface';
import { GraphQLService } from './base-api.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService extends GraphQLService {

    constructor(public _http: HttpClient) {
        super(_http);
    }

    public refreshToken(token: string): Observable<TokenRenewal>  {

        // used http://jwtbuilder.jamiekurtz.com/ to create dummy token
        const REFRESHTOKEN = `
        mutation RefreshAuthToken {
            refreshJwtAuthToken(  input: {
                jwtRefreshToken: "${token}",
            }) {
                authToken
                }
        }`

        return this.mutate({
            mutation: REFRESHTOKEN
        });
    }

    public authenticateUser({username, password}: AuthLoginUser): Observable<LoginUserResponseQuery> {
        const LOGIN = `
            mutation LoginUser {
                login( input: {
                username: "${username}",
                password: "${password}"
                } ) {
                authToken
                refreshToken
                user {
                    username
                    userId
                    avatar {
                        url
                    }
                    email
                    databaseId
                    capabilities
                }
                }
            }
        `
        return this.mutate({
            mutation: LOGIN
        });
    }
    public registerUser({username, email, password}:  AuthRegisterUser): Observable<RegisterUserResponseQuery> {
        const REGISTER = `
            mutation MyMutation {
                __typename
                registerUser(input: {username: "${username}", email: "${email}", password: "${password}"}) {
                    authToken
                    refreshToken
                    user {
                        username
                        userId
                        avatar {
                            url
                        }
                        email
                        databaseId
                        capabilities
                    }
                    clientMutationId
                }
            }
        `
        return this.mutate({
            mutation: REGISTER
        });
    }
}
