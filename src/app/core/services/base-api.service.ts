
import { Config } from './../config/app.config';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

export class GraphQLService {

    protected _endpoint: string = '';
    protected _domain: string = Config.api_base;
    // protected _version: string | number = 1

    constructor(public _http: HttpClient) {

    }

    public query<T>(options: {
        query: string;
        variables?: { [key: string]: any };
    }): Observable<T> {
        return this._http
            .post<{ data: T }>(`${Config.api_base}`, {
                query: options.query,
                variables: options.variables,
            })
            .pipe(map((d) => {
                // console.log('info: ', d);
                return d.data;
            }));
    }

    public mutate<T>(options: {
        mutation: string;
        variables?: { [key: string]: any };
    }): Observable<any> {
        return this._http
            .post<{ data: T }>(`${Config.api_base}`, {
                query: options.mutation,
                variables: options.variables,
            })
            .pipe(map((d) => d.data));
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            return of(result as T);
        };
    }

}
