import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GraphQLService } from './base-api.service';
import { frontPageDetailsQuery, pageQuery } from 'src/app/types/types';


@Injectable({
    providedIn: 'root'
})
export class BlogPageService extends GraphQLService {

    constructor(public _http: HttpClient, private store: Store) {
        super(_http);
    }

    private featuredImageQuery =
    `
        featuredImage {
            node {
                link
                srcSet
                altText
                sourceUrl
            }
        }
    `

    public fetchAllPages(): Observable<pageQuery> {
        const pageQueryPayload =
            `
            {
                pages(first: 20) {
                    edges {
                        node {
                            id
                            isFrontPage
                            slug
                            content
                            title
                            databaseId
                            ${this.featuredImageQuery}
                        }
                    }
                }
            }
        `
        return this.query({
            query: pageQueryPayload
        });
    }

    public fetchFrontPage(id): Observable<frontPageDetailsQuery> {
        const frontPageQueryPayload =
            `
                {
                    page(id: "${id}") {
                        pageId
                        id
                        isFrontPage
                        slug
                        title
                        databaseId
                        postsGroups {
                            fieldGroupName
                            categoryPosts {
                                fieldGroupName
                                financesSection {
                                    fieldGroupName
                                    sectionTitle
                                    posts {
                                        ... on Post {
                                            id
                                            slug
                                            title
                                            categories {
                                                nodes {
                                                    id
                                                    categoryId
                                                    count
                                                    databaseId
                                                    description
                                                    isRestricted
                                                    name
                                                    slug
                                                }
                                            }
                                            content
                                            excerpt
                                            modified
                                            commentCount
                                            date
                                            ${this.featuredImageQuery}
                                        }
                                    }
                                }
                                investmentSection {
                                    fieldGroupName
                                    sectionTitle
                                    posts {
                                        ... on Post {
                                            id
                                            slug
                                            title
                                            categories {
                                                nodes {
                                                    id
                                                    categoryId
                                                    count
                                                    databaseId
                                                    description
                                                    isRestricted
                                                    name
                                                    slug
                                                }
                                            }
                                            content
                                            excerpt
                                            modified
                                            commentCount
                                            date
                                            ${this.featuredImageQuery}
                                            modified
                                        }
                                    }
                                }
                                propertySection {
                                    fieldGroupName
                                    sectionTitle
                                    posts {
                                        ... on Post {
                                            id
                                            slug
                                            title
                                            categories {
                                                nodes {
                                                    id
                                                    categoryId
                                                    count
                                                    databaseId
                                                    description
                                                    isRestricted
                                                    name
                                                    slug
                                                }
                                            }
                                            content
                                            excerpt
                                            modified
                                            commentCount
                                            date
                                            ${this.featuredImageQuery}
                                            modified
                                        }
                                    }
                                }
                                taxSection {
                                    fieldGroupName
                                    sectionTitle
                                    posts {
                                        ... on Post {
                                            id
                                            slug
                                            title
                                            categories {
                                                nodes {
                                                    id
                                                    categoryId
                                                    count
                                                    databaseId
                                                    description
                                                    isRestricted
                                                    name
                                                    slug
                                                }
                                            }
                                            content
                                            excerpt
                                            modified
                                            commentCount
                                            date
                                            ${this.featuredImageQuery}
                                            modified
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            `
            return this.query({
                query: frontPageQueryPayload
            });
    }
}
