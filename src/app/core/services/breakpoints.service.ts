import {Injectable} from '@angular/core';

export const CustomBreakpointNames = {
  mobileLarge: 'mobileLarge',
  mobile: 'mobile',
  ipad: 'ipad',
  ipadPro: 'ipadPro',
  desktop: 'desktop',
};

@Injectable({
  providedIn: 'root'
})
export class BreakpointsService {
  breakpoints: object = {
    '(min-width: 375px)': CustomBreakpointNames.mobile,
    '(min-width: 428px)': CustomBreakpointNames.mobileLarge,
    '(min-width: 720px)': CustomBreakpointNames.ipad,
    '(min-width: 1024px)': CustomBreakpointNames.ipadPro,
    '(min-width: 1440px)': CustomBreakpointNames.desktop
  };

  getBreakpoints(): string[] {
    return Object.keys(this.breakpoints);
  }

  getBreakpointName(breakpointValue): string {
    return this.breakpoints[breakpointValue];
  }

  constructor() {
  }
}