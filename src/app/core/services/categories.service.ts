import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GraphQLService } from './base-api.service';
import { categoriesQuery } from 'src/app/types/types';


@Injectable({
    providedIn: 'root'
})
export class CategoriesService extends GraphQLService {

    constructor(public _http: HttpClient, private store: Store) {
        super(_http);
    }

    public fetchAllCategories(): Observable<categoriesQuery> {
        const categoriesQueryPayload =
        `
            {
                categories {
                edges {
                    node {
                        id
                        databaseId
                        count
                        description
                        isRestricted
                        name
                        slug
                    }
                }
            }
        }
    `
    return this.query({
            query: categoriesQueryPayload
        });
    }
}
