import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable, of } from 'rxjs';
import { AppRoute } from '../interfaces/app-route.interface';
import { GraphQLService } from './base-api.service';
import { menuQuery, MenusQuery } from 'src/app/types/types';
import { FetchNavItemsSuccess, SetSelectedNavItemSuccess } from '../store/actions/navigation.actions';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class CoreService extends GraphQLService {
    private _token: string;

    constructor(public _http: HttpClient, private store: Store) {
        super(_http);
    }

    public getAllNavigationMenues(): Observable<MenusQuery> {
        const menuQueryPayload =
        `
        {
            menus {
                edges {
                    node {
                        databaseId
                        name
                        slug
                        locations
                        menuItems {
                            edges {
                                node {
                                    path
                                    label
                                    id
                                }
                            }
                        }
                    }
                }
            }
        }
        `
        return this.query({
            query: menuQueryPayload
        });

    }
}
