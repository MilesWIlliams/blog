import { AuthService } from './auth.service';
import { BlogPageService } from './blog-page.service';
import { BreakpointsService } from './breakpoints.service';
import { CategoriesService } from './categories.service';
import { CoreService } from './core.service';
import { LayoutService } from './layout.service';
import { PostsService } from './posts.service';
import { ReviewsService } from './reviews.service';
import { TagsService } from './tag.service';
import { UIDService } from './uid.service';
import { UsersService } from './user.service';

export const services: any[] = [
    CoreService,
    BlogPageService,
    UIDService,
    AuthService,
    BreakpointsService,
    LayoutService,
    CategoriesService,
    TagsService,
    PostsService,
    UsersService,
    ReviewsService
];
