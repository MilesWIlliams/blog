import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GraphQLService } from './base-api.service';
import { categoriesQuery, PostsQuery, postsByCategoryQuery, postsByIDQuery, postsQuery } from 'src/app/types/types';
import { PostCommentPayload } from '../interfaces/comment.interface';


@Injectable({
    providedIn: 'root'
})
export class PostsService extends GraphQLService {

    private authorQuery = `
        author {
            node {
                databaseId
                description
                name
                id
                ... on User {
                    id
                    email
                    avatar {
                        url
                        foundAvatar
                        scheme
                    }
                }
            }
        }
    `

    private commentAuthorQuery = `
        author {
            node {
                id
                url
                name
                ... on User {
                    id
                    email
                    avatar {
                        url
                    }
                }
            }
        }
    `

    private categoriesQuery = `
        categories {
            edges {
                node {
                    id
                    count
                    databaseId
                    description
                    isRestricted
                    name
                    slug
                }
            }
        }
    `
    private tagsQuery = `
        tags {
            edges {
                node {
                    slug
                    tagId
                    name
                }
            }
        }
    `

    private pageInfoQuery = `
        pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
        }
    `

    private paginationQuery = `
        next {
            slug
        }
        previous {
            slug
        }
    `

    constructor(public _http: HttpClient, private store: Store) {
        super(_http);
    }

    public fetchAllPosts(): Observable<postsQuery> {
        const categoriesQueryPayload =
        `
            {
                posts(first:10) {
                    ${this.pageInfoQuery}
                    edges {
                        node {
                            id
                            authorId
                            author {
                                node {
                                    id
                                    name
                                    description
                                    databaseId
                                    avatar {
                                        url
                                    }
                                }
                            }
                            databaseId
                            date
                            categories {
                                edges {
                                    node {
                                        id
                                        count
                                        databaseId
                                        description
                                        isRestricted
                                        name
                                        slug
                                    }
                                }
                            }
                            commentCount
                            content(format: RENDERED)
                            comments {
                                nodes {
                                    databaseId
                                    date
                                    id
                                    approved
                                    content
                                    author {
                                        node {
                                            databaseId
                                            name
                                            id
                                            ... on User {
                                                id
                                                email
                                                avatar {
                                                    url
                                                    foundAvatar
                                                    scheme
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            excerpt
                            featuredImageId
                            featuredImage {
                                node {
                                    altText
                                    uri
                                    srcSet
                                    sourceUrl
                                    link
                                    title
                                    slug
                                }
                            }
                            isPreview
                            isRestricted
                            ${this.paginationQuery}
                            relatedPosts(where: {limit: 10}) {
                                edges {
                                    node {
                                        id
                                        title(format: RENDERED)
                                        slug
                                        featuredImage {
                                            node {
                                                uri
                                                srcSet
                                                link
                                                title
                                                sourceUrl
                                                slug
                                            }
                                        }
                                        comments {
                                            nodes {
                                                databaseId
                                                date
                                                id
                                                approved
                                                content
                                                author {
                                                    node {
                                                        databaseId
                                                        name
                                                        id
                                                        ... on User {
                                                            id
                                                            email
                                                            avatar {
                                                                url
                                                                foundAvatar
                                                                scheme
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ${this.tagsQuery}
                            title(format: RENDERED)
                            slug
                        }
                        cursor
                    }
                }

            }
        `
        return this.query({
            query: categoriesQueryPayload
        });
    }
    public fetchPostsbyCategoryId(id): Observable<postsByCategoryQuery> {
        const categoriesQueryPayload =
        `
            {
                category(id: "${id}") {
                    posts(first:5) {
                        ${this.pageInfoQuery}
                        edges {
                            node {
                                id
                                authorId
                                author {
                                    node {
                                        id
                                        name
                                        description
                                        databaseId
                                        avatar {
                                            url
                                        }
                                    }
                                }
                                databaseId
                                date
                                categories {
                                    edges {
                                        node {
                                            id
                                            count
                                            databaseId
                                            description
                                            isRestricted
                                            name
                                            slug
                                        }
                                    }
                                }
                                commentCount
                                content(format: RENDERED)
                                comments {
                                    nodes {
                                        databaseId
                                        date
                                        id
                                        approved
                                        content
                                        author {
                                            node {
                                                databaseId
                                                name
                                                id
                                                ... on User {
                                                    id
                                                    email
                                                    avatar {
                                                        url
                                                        foundAvatar
                                                        scheme
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                excerpt
                                featuredImageId
                                featuredImage {
                                    node {
                                        altText
                                        uri
                                        srcSet
                                        sourceUrl
                                        link
                                        title
                                        slug
                                    }
                                }
                                isPreview
                                isRestricted
                                ${this.paginationQuery}
                                relatedPosts(where: {limit: 10}) {
                                    edges {
                                        node {
                                            id
                                            title(format: RENDERED)
                                            slug
                                            featuredImage {
                                                node {
                                                    uri
                                                    srcSet
                                                    link
                                                    title
                                                    sourceUrl
                                                    slug
                                                }
                                            }
                                            comments {
                                                nodes {
                                                    databaseId
                                                    date
                                                    id
                                                    approved
                                                    content
                                                    author {
                                                        node {
                                                            databaseId
                                                            name
                                                            id
                                                            ... on User {
                                                                id
                                                                email
                                                                avatar {
                                                                    url
                                                                    foundAvatar
                                                                    scheme
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ${this.tagsQuery}
                                title(format: RENDERED)
                                slug
                            }
                            cursor
                        }
                    }
                }
            }
        `
        return this.query({
            query: categoriesQueryPayload
        });
    }


    public fetchPostsbyPageId(id): Observable<postsByCategoryQuery> {
        const pageQueryPayload =
        `
            {
                page(id: "${id}") {
                    posts {
                        edges {
                            node {
                                id
                                authorId
                                author {
                                    node {
                                        id
                                        name
                                        description
                                        databaseId
                                        avatar {
                                            url
                                        }
                                    }
                                }
                                databaseId
                                date
                                categories {
                                    edges {
                                        node {
                                            id
                                            count
                                            databaseId
                                            description
                                            isRestricted
                                            name
                                            slug
                                        }
                                    }
                                }
                                comments {
                                    nodes {
                                        databaseId
                                        date
                                        id
                                        approved
                                        content
                                        author {
                                            node {
                                                databaseId
                                                name
                                                id
                                                ... on User {
                                                    id
                                                    email
                                                    avatar {
                                                        url
                                                        foundAvatar
                                                        scheme
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                content(format: RENDERED)
                                featuredImageId
                                isPreview
                                isRestricted
                                ${this.paginationQuery}
                                title(format: RENDERED)
                                slug
                                featuredImage {
                                    node {
                                        uri
                                        srcSet
                                        link
                                        title
                                        slug
                                    }
                                }
                            }
                            cursor
                        }
                    }
                }
            }
        `
        return this.query({
            query: pageQueryPayload
        });
    }

    public fetchPostsbyId(slug): Observable<postsByIDQuery> {
        const postQueryPayload =
        `{
            postBy(slug:"${slug}") {
                id
                authorId
                ${this.authorQuery}
                databaseId
                date
                ${this.categoriesQuery}
                commentCount
                comments {
                    edges {
                        node {
                            databaseId
                            date
                            id
                            approved
                            content
                            ${this.commentAuthorQuery}
                        }
                    }
                }
                content(format: RENDERED)
                excerpt
                featuredImageId
                featuredImage {
                    node {
                        uri
                        srcSet
                        link
                        title
                        slug
                    }
                }
                isPreview
                isRestricted
                ${this.paginationQuery}
                relatedPosts(where: {limit: 10}) {
                    edges {
                        node {
                            id
                            title(format: RENDERED)
                            slug
                            featuredImage {
                                node {
                                    uri
                                    srcSet
                                    sourceUrl
                                    link
                                    title
                                    slug
                                }
                            }
                        }
                    }
                }
                ${this.tagsQuery}
                title(format: RENDERED)
                slug
            }
        }
        `
        return this.query({
            query: postQueryPayload
        });
    }

    public fetchLatestPosts(id): Observable<postsByCategoryQuery> {
        const pageQueryPayload =
        `
            {
                page(first: 10) {
                    posts {
                        edges {
                            node {
                                id
                                authorId
                                author {
                                    node {
                                        id
                                        name
                                        databaseId
                                    }
                                }
                                databaseId
                                date
                                ${this.categoriesQuery}
                                commentCount
                                comments {
                                    nodes {
                                        databaseId
                                        date
                                        id
                                        approved
                                        content
                                        author {
                                            node {
                                                databaseId
                                                name
                                                id
                                                ... on User {
                                                    id
                                                    email
                                                    avatar {
                                                        url
                                                        foundAvatar
                                                        scheme
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                content(format: RENDERED)
                                excerpt
                                featuredImageId
                                isPreview
                                isRestricted
                                ${this.paginationQuery}
                                title(format: RENDERED)
                                slug
                                ${this.tagsQuery}
                                featuredImage {
                                    node {
                                        uri
                                        srcSet
                                        link
                                        title
                                        slug
                                    }
                                }
                            }
                            cursor
                        }
                    }
                }
            }
        `
        return this.query({
            query: pageQueryPayload
        });
    }

    public postComment(config: PostCommentPayload) {

        const makeCommentPayload =
        `
            mutation CREATE_COMMENT {
                createComment(input: {commentOn: ${config.databaseId}, content: "${config.content}", author: "${config.author}", authorEmail: "${config.authorEmail}"}) {
                    success
                    comment {
                        id
                        content
                        date
                        databaseId
                        approved
                        ${this.commentAuthorQuery}
                    }
                }
            }
        `

        return this.mutate({
            mutation: makeCommentPayload
        });
    }

    public searchPosts(query: string): Observable<PostsQuery> {
        const categoriesQueryPayload =
        `
            {
                posts(where: {search: "${query}"}) {
                    edges {
                        node {
                            id
                            authorId
                            author {
                                node {
                                    id
                                    name
                                    description
                                    databaseId
                                    avatar {
                                        url
                                    }
                                }
                            }
                            databaseId
                            date
                            categories {
                                edges {
                                    node {
                                        id
                                        count
                                        databaseId
                                        description
                                        isRestricted
                                        name
                                        slug
                                    }
                                }
                            }
                            commentCount
                            content(format: RENDERED)
                            comments {
                                nodes {
                                    databaseId
                                    date
                                    id
                                    approved
                                    content
                                    author {
                                        node {
                                            databaseId
                                            name
                                            id
                                            ... on User {
                                                id
                                                email
                                                avatar {
                                                    url
                                                    foundAvatar
                                                    scheme
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            excerpt
                            featuredImageId
                            featuredImage {
                                node {
                                    altText
                                    uri
                                    srcSet
                                    sourceUrl
                                    link
                                    title
                                    slug
                                }
                            }
                            isPreview
                            isRestricted
                            ${this.paginationQuery}
                            relatedPosts(where: {limit: 10}) {
                                edges {
                                    node {
                                        id
                                        title(format: RENDERED)
                                        slug
                                        featuredImage {
                                            node {
                                                uri
                                                srcSet
                                                sourceUrl
                                                link
                                                title
                                                slug
                                            }
                                        }
                                        ${this.paginationQuery}
                                        comments {
                                            nodes {
                                                databaseId
                                                date
                                                id
                                                approved
                                                content
                                                author {
                                                    node {
                                                        databaseId
                                                        name
                                                        id
                                                        ... on User {
                                                            id
                                                            email
                                                            avatar {
                                                                url
                                                                foundAvatar
                                                                scheme
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ${this.tagsQuery}
                            title(format: RENDERED)
                            slug
                        }
                        cursor
                    }
                }

            }
        `
        return this.query({
            query: categoriesQueryPayload
        });
    }

    public getFeaturedPosts(): Observable<PostsQuery> {
        const query =`
            {
                __typename
                posts(first: 10, where: {categoryName: "featured"}) {
                    edges {
                        node {
                            id
                            authorId
                            author {
                                node {
                                    id
                                    name
                                    description
                                    databaseId
                                    avatar {
                                        url
                                    }
                                }
                            }
                            databaseId
                            date
                            ${this.categoriesQuery}
                            commentCount
                            comments {
                                nodes {
                                    databaseId
                                    date
                                    id
                                    approved
                                    content
                                    author {
                                        node {
                                            databaseId
                                            name
                                            id
                                            ... on User {
                                                id
                                                email
                                                avatar {
                                                    url
                                                    foundAvatar
                                                    scheme
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            content(format: RENDERED)
                            excerpt
                            featuredImageId
                            isPreview
                            isRestricted
                            ${this.paginationQuery}
                            title(format: RENDERED)
                            slug
                            ${this.tagsQuery}
                            featuredImage {
                                node {
                                    uri
                                    srcSet
                                    sourceUrl
                                    link
                                    title
                                    slug
                                }
                            }
                        }
                        cursor
                    }
                }
        }
        `
        return this.query({
            query: query
        });
    }

    public getPostsByTag(tag: string): Observable<PostsQuery> {
        const query =`
            {
                __typename
                posts(first: 10, where: {tag: "${tag}"}) {
                    edges {
                        node {
                            id
                            authorId
                            author {
                                node {
                                    id
                                    name
                                    description
                                    databaseId
                                    avatar {
                                        url
                                    }
                                }
                            }
                            databaseId
                            date
                            ${this.categoriesQuery}
                            commentCount
                            comments {
                                nodes {
                                    databaseId
                                    date
                                    id
                                    approved
                                    content
                                    author {
                                        node {
                                            databaseId
                                            name
                                            id
                                            ... on User {
                                                id
                                                email
                                                avatar {
                                                    url
                                                    foundAvatar
                                                    scheme
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            content(format: RENDERED)
                            excerpt
                            featuredImageId
                            isPreview
                            isRestricted
                            ${this.paginationQuery}
                            ${this.tagsQuery}
                            title(format: RENDERED)
                            slug
                            featuredImage {
                                node {
                                    uri
                                    srcSet
                                    sourceUrl
                                    link
                                    title
                                    slug
                                }
                            }
                        }
                        cursor
                    }
                }
        }
        `
        return this.query({
            query: query
        });
    }
}
