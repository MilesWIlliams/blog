import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GraphQLService } from './base-api.service';
import { ReviewBySlugQuery, ReviewsQuery } from 'src/app/types/types';


@Injectable({
    providedIn: 'root'
})
export class ReviewsService extends GraphQLService {

    constructor(public _http: HttpClient, private store: Store) {
        super(_http);
    }

    public fetchReviews(): Observable<ReviewsQuery> {
        const query = `
        {
            __typename
            reviews(first: 10) {
                pageInfo {
                    endCursor
                    hasNextPage
                    hasPreviousPage
                    startCursor
                }
                nodes {
                authorId
                author {
                    node {
                        name
                        avatar {
                            url
                        }
                        description
                    }
                }
                date
                databaseId
                excerpt
                review {
                    rating
                    takeawaySummary
                    companyDetailsGroup {
                        type
                        fieldGroupName
                        affiliateDetails {
                            affiliateButtonText
                            affiliateLink
                            fieldGroupName
                        }
                        companysDetails {
                            productPricingDetails {
                                feesOrPriceLabelText
                                fieldGroupName
                                price
                            }
                            promotionsDetails {
                                fieldGroupName
                                promotionsLink
                                promotionsLinkText
                            }
                            startDate
                            websiteUrl
                        }
                        logo {
                            altText
                            caption
                            databaseId
                            date
                            mediaType
                            srcSet
                            sourceUrl
                            title
                        }
                        prosAndCons {
                            cons {
                                con1
                                con2
                                con3
                                con4
                                con5
                            }
                            pros {
                                pro1
                                pro2
                                pro3
                                pro4
                                pro5
                            }
                        }
                    }
                    fullReview {
                        theBad {
                            content
                            heading
                        }
                        theGood {
                            content
                            heading
                        }
                    }
                    media {
                        image1 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image10 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image2 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image3 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image4 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image5 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image6 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image7 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image8 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image9 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                    }
                }
                slug
                title
                status
                }
            }
            }
        `
        return this.query({
            query: query
        });
    }

    public fetchReviewBySlug(slug): Observable<ReviewBySlugQuery> {
        const query = `
        {
            reviewBy(slug: "${slug}") {
                authorId
                author {
                    node {
                        name
                        description
                        avatar {
                            url
                        }
                    }
                }
                date
                databaseId
                excerpt
                review {
                    rating
                    takeawaySummary
                    companyDetailsGroup {
                        type
                        fieldGroupName
                        affiliateDetails {
                            affiliateButtonText
                            affiliateLink
                            fieldGroupName
                        }
                        companysDetails {
                            productPricingDetails {
                                feesOrPriceLabelText
                                fieldGroupName
                                price
                            }
                            promotionsDetails {
                                fieldGroupName
                                promotionsLink
                                promotionsLinkText
                            }
                            startDate
                            websiteUrl
                        }
                        logo {
                            altText
                            caption
                            databaseId
                            date
                            mediaType
                            srcSet
                            sourceUrl
                            title
                        }
                        prosAndCons {
                            cons {
                                con1
                                con2
                                con3
                                con4
                                con5
                            }
                            pros {
                                pro1
                                pro2
                                pro3
                                pro4
                                pro5
                            }
                        }
                    }
                    fullReview {
                        theBad {
                            content
                            heading
                        }
                        theGood {
                            content
                            heading
                        }
                    }
                    media {
                        image1 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image10 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image2 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image3 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image4 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image5 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image6 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image7 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image8 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                        image9 {
                            srcSet
                            sourceUrl
                            title
                            altText
                        }
                    }
                }
                slug
                title
                status
            }
        }
        `
        return this.query({
            query: query
        });
    }
}
