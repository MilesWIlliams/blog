import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GraphQLService } from './base-api.service';
import { tagQuery } from 'src/app/types/types';


@Injectable({
    providedIn: 'root'
})
export class TagsService extends GraphQLService {

    constructor(public _http: HttpClient, private store: Store) {
        super(_http);
    }

    public fetchAllTags(): Observable<tagQuery> {
        const tagQueryPayload =
        `
            {
                tags {
                edges {
                    node {
                        id
                        count
                        description
                        isRestricted
                        name
                        slug
                        tagId
                    }
                }
            }
        }
    `
    return this.query({
            query: tagQueryPayload
        });
    }
}
