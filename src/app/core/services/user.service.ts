import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GraphQLService } from './base-api.service';
import { categoriesQuery } from 'src/app/types/types';
import { LoginUserResponseQuery, RegisterUserResponseQuery, userByIDQuery, usersQuery } from 'src/app/types/users.type';


@Injectable({
    providedIn: 'root'
})
export class UsersService extends GraphQLService {

    constructor(public _http: HttpClient, private store: Store) {
        super(_http);
    }

    public fetchUserByID(userDatabaseId: number): Observable<userByIDQuery> {
        const getUserQueryPayload =
            `
            {
                user(id: "${userDatabaseId}" idType: DATABASE_ID) {
                    avatar {
                        url
                    }
                    name
                    firstName
                    name
                    username
                    id
                    databaseId
                    date
                    description
                }
            }
        `
        return this.query({
            query: getUserQueryPayload
        });
    }

    public fetchAllUsers(): Observable<usersQuery> {
        const getAllUsersQueryPayload =
        `
            {
                users {
                    edges {
                        node {
                            firstName
                            name
                            username
                            id
                            databaseId
                            description
                        }
                    }
                }
            }
        `
        return this.query({
            query: getAllUsersQueryPayload
        });
    }

    public loginUser({username, password}): Observable<LoginUserResponseQuery> {
        const LOGIN = `
            mutation LoginUser {
                login( input: {
                username: "${username}",
                password: "${password}"
                } ) {
                authToken
                user {
                    username
                    userId
                    avatar {
                        url
                    }
                    email
                    databaseId
                    capabilities
                }
                }
            }
        `
        return this.mutate({
            mutation: LOGIN
        });
    }
    public registerUser({username, email, password}): Observable<RegisterUserResponseQuery> {
        const REGISTER = `
            mutation MyMutation {
                __typename
                registerUser(input: {username: "${username}", email: "${email}", password: "${password}"}) {
                    user {
                        username
                        userId
                        avatar {
                            url
                        }
                        email
                        databaseId
                        capabilities
                    }
                    clientMutationId
                }
            }
        `
        return this.mutate({
            mutation: REGISTER
        });
    }
}
