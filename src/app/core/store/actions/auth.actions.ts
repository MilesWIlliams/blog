import { Utils } from 'src/app/utils';
import { AuthLoginUser, AuthPaylod, TokenRenewal, AuthRegisterUser } from '../../interfaces/auth.interface';

export class AuthenticateUser {
	static readonly type = Utils.Helpers.Type('[Auth: Login] Authenticate User');
	constructor(public readonly payload: AuthLoginUser) { }
}

export class AuthenticateUserSuccess {
	static readonly type = Utils.Helpers.Type('[Auth: Login] Authenticate User Success');
	constructor(public readonly payload: AuthPaylod) { }
}

export class AuthenticateUserFail {
	static readonly type = Utils.Helpers.Type('[Auth: Login] Authenticate User fail');
	constructor(public readonly payload: any) { }
}
export class RegisterUser {
	static readonly type = Utils.Helpers.Type('[Auth: Login] Register User');
	constructor(public readonly payload: AuthRegisterUser) { }
}

export class RegisterUserSuccess {
	static readonly type = Utils.Helpers.Type('[Auth: Login] Register User Success');
	constructor(public readonly payload: AuthPaylod) { }
}

export class RegisterUserFail {
	static readonly type = Utils.Helpers.Type('[Auth: Login] Register User fail');
	constructor(public readonly payload: any) { }
}

export class RefreshToken {
	static readonly type = Utils.Helpers.Type('[Auth: Token] Token renewal');
	constructor(public readonly payload: TokenRenewal) {}
}

export class RefreshTokenSuccess {
	static readonly type = Utils.Helpers.Type('[Auth: Token] Token renewal success');
	constructor(public readonly payload: TokenRenewal) {}
}

export class RefreshTokenFail {
	static readonly type = Utils.Helpers.Type('[Auth: Token] Token renewal fail');
	constructor(public readonly payload: any) {}
}
