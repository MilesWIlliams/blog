import { Utils } from "src/app/utils";

export class AssignDeviceLayout  {
    static readonly type = Utils.Helpers.Type('[Device: Layout] Assign device layout');
    constructor(public readonly payload: string[]) {}
}