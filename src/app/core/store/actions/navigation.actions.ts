import { Utils } from 'src/app/utils';
import { AppMenu, AppRoute } from '../../interfaces/app-route.interface';


export class FetchNavItems  {
    static readonly type = Utils.Helpers.Type('[Nav: Fetch] Fetch nav items');
    constructor() {}
}

export class FetchNavItemsSuccess  {
    static readonly type = Utils.Helpers.Type('[Nav: Fetch] Fetch nav items success');
    constructor(public readonly payload: AppMenu[]) {}
}

export class FetchNavItemsFail  {
    static readonly type = Utils.Helpers.Type('[Nav: Fetch] Fetch nav items fail');
    constructor(public readonly payload: any) {}
}

export class SetSelectedNavItem  {
    static readonly type = Utils.Helpers.Type('[Nav: Select] Set selected nav item');
    constructor(public readonly payload: string) {
    }
}

export class SetSelectedNavItemSuccess  {
    static readonly type = Utils.Helpers.Type('[Nav: Select] Set selected nav item success');
    constructor(public readonly payload: string) {}
}

export class SetSelectedNavItemFail  {
    static readonly type = Utils.Helpers.Type('[Nav: Select] Set selected nav item fail');
    constructor(public readonly payload: any) {}
}

export class UpdateURL {
    static readonly type = Utils.Helpers.Type('[Nav: Update] Update url');
    constructor(public readonly payload: string) {}
}
