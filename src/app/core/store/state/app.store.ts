import { State, Selector, Store, Action, StateContext, createSelector, NgxsOnInit } from '@ngxs/store';
import { Injectable } from '@angular/core';
import * as authActions from '../actions/auth.actions';
import * as navActions from '../actions/navigation.actions';

import { AuthService } from '../../services/auth.service';
import { tap } from 'rxjs/operators';
import { Navigate } from '@ngxs/router-plugin';
import { AppUser } from '../../interfaces/app-user.interface';
import { AssignDeviceLayout } from '../actions/layout.actions';
import { AppMenu, AppRoute } from '../../interfaces/app-route.interface';
import { Utils } from 'src/app/utils';
import { CoreService } from '../../services/core.service';
import { menuQuery, MenusQuery } from 'src/app/types/types';
import { Router } from '@angular/router';
import { NavigationState } from './navigation.store';


export interface AppStateModel {
    loading: boolean;
    loaded: boolean;
    isAuthenticated: boolean;
    user: string,
    token: string;
    layout: string[];
}

@State<AppStateModel>({
    name: 'app',
    defaults: {
        loading: false,
        loaded: false,
        isAuthenticated: false,
        user: null,
        token: null,
        layout: null,
    }
})
@Injectable()
export class AppState {
    private cryptor = new Utils.Helpers.Encryptor();
    constructor(private store: Store, private _svc: AuthService, private _coreSvc: CoreService, private router: Router) { }


    @Selector()
    static isLoading(state: AppStateModel) { return state.loading; }

    @Selector()
    static hasLoaded(state: AppStateModel) { return state.loaded; }

    @Selector()
    static isAuthenticated(state: AppStateModel) { return state.isAuthenticated; }

    @Selector()
    static Token(state: AppStateModel) { return state.token; }

    @Selector()
    static user(state: AppStateModel): AppUser {

        return JSON.parse(new Utils.Helpers.Encryptor().decrypt(state.user));
    }

    @Selector()
    static isBreakpointActive(breakpointName) {
        return createSelector([AppState], (state: AppStateModel) => {

            return state.layout.find(breakpoint => breakpoint === breakpointName);
        })
    }

    @Selector()
    static activeBreakpoint(state: AppStateModel) { return state.layout[0]; }

    @Action(AssignDeviceLayout)
    assignDeviceLayout(ctx: StateContext<AppStateModel>, action: AssignDeviceLayout) {
        ctx.patchState({
            layout: action.payload
        });
    }

    @Action(authActions.AuthenticateUser)
    authenticateUser(ctx: StateContext<AppStateModel>, action: authActions.AuthenticateUser) {
        const userCredentials = action.payload;
        ctx.patchState({
            loading: true,
            loaded: false
        });

        return this._svc.authenticateUser(userCredentials).pipe(
            tap(
                res => ctx.dispatch(new authActions.AuthenticateUserSuccess({ auth_token: res.login.authToken, refresh_token: res.login.refreshToken, user: res.login.user })),
                err => ctx.dispatch(new authActions.AuthenticateUserFail(err))
            )
        );
    }

    @Action(authActions.AuthenticateUserSuccess)
    authenticateUserSuccess(ctx: StateContext<AppStateModel>, action: authActions.AuthenticateUserSuccess) {
        const ecryptedUser = this.cryptor.encrypt(JSON.stringify(action.payload.user));

        ctx.patchState({
            loading: false,
            loaded: true,
            user: ecryptedUser,
            isAuthenticated: true,
            token: action.payload.refresh_token
        });
        const previous_url: string = this.store.selectSnapshot(NavigationState.getPreviousUrl);
        console.log('NavigationState.getPreviousUrl', previous_url);

        return ctx.dispatch(new Navigate([previous_url]));
    }

    @Action(authActions.AuthenticateUserFail)
    authenticateUserFail(ctx: StateContext<AppStateModel>, action: authActions.AuthenticateUserFail) {
        ctx.patchState({
            loading: false,
            loaded: false,
        });
    }

    @Action(authActions.RegisterUser)
    registerUser(ctx: StateContext<AppStateModel>, action: authActions.RegisterUser) {

        return this._svc.registerUser(action.payload).pipe(
            tap(
                (res) => ctx.dispatch([
                    new authActions.RegisterUserSuccess({ user: res.registerUser.user, auth_token: res.registerUser.authToken, refresh_token: res.registerUser.refreshToken }),
                    new Navigate(['/'])
                ]),
                (err: any) => ctx.dispatch(new authActions.RegisterUserFail(err))
            )
        );
    }

    @Action(authActions.RegisterUserSuccess)
    registerUserSuccess(ctx: StateContext<AppStateModel>, action: authActions.RegisterUserSuccess) {
        const registerResponse = action.payload;
        const ecryptedUser = this.cryptor.encrypt(JSON.stringify(action.payload.user));
        ctx.patchState({
            loaded: true,
            loading: false,
            user: ecryptedUser,
            isAuthenticated: true,
            token: registerResponse.refresh_token
        });
    }

    @Action(authActions.RefreshToken)
    refreshToken(ctx: StateContext<AppStateModel>, action: authActions.RefreshToken) {

        return this._svc.refreshToken(ctx.getState().token).pipe(
            tap(
                res => ctx.dispatch(new authActions.RefreshTokenSuccess(res)),
                err => ctx.dispatch(new authActions.RefreshTokenFail(err))
            )
        );
    }

    @Action(authActions.RefreshTokenSuccess)
    refreshTokenSuccess(ctx: StateContext<AppStateModel>, action: authActions.RefreshTokenSuccess) {
        ctx.patchState({
            token: action.payload.token
        });
    }
}