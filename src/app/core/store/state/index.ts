import { AppState } from "./app.store";
import { NavigationState } from "./navigation.store";

export const coreStates = [AppState, NavigationState]