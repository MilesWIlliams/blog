import { State, Selector, Store, Action, StateContext, createSelector, NgxsOnInit } from '@ngxs/store';
import { Injectable } from '@angular/core';
import * as navActions from '../actions/navigation.actions';
import { tap } from 'rxjs/operators';
import { AppMenu, AppRoute } from '../../interfaces/app-route.interface';
import { Utils } from 'src/app/utils';
import { CoreService } from '../../services/core.service';
import { menuQuery, MenusQuery } from 'src/app/types/types';
import { Router } from '@angular/router';


export interface NavigationStateModel {
    loading: boolean;
    loaded: boolean;
    current_url: string;
    previous_url: string;
    menus: { [location: string]: AppMenu }
    nav_loaded: boolean;
    selected_nav: string,
    selected_nav_loading: boolean,
    selected_nav_loaded: boolean
}

@State<NavigationStateModel>({
    name: 'navigation',
    defaults: {
        loading: false,
        loaded: false,
        current_url: '/',
        previous_url: '/',
        menus: {},
        nav_loaded: false,
        selected_nav: '',
        selected_nav_loading: false,
        selected_nav_loaded: false
    }
})
@Injectable()
export class NavigationState implements NgxsOnInit {
    constructor(private store: Store, private _coreSvc: CoreService, private router: Router) { }

    ngxsOnInit() {
        this.store.dispatch(new navActions.FetchNavItems());
    }

    @Selector()
    static isLoading(state: NavigationStateModel) { return state.loading; }

    @Selector()
    static hasLoaded(state: NavigationStateModel) { return state.loaded; }

    @Selector()
    static navLoaded(state: NavigationStateModel) { return state.nav_loaded; }

    @Selector()
    static getSelectedNavID(state: NavigationStateModel) { return state.selected_nav }

    @Selector()
    static getPreviousUrl(state: NavigationStateModel) { return state.previous_url }

    @Selector()
    static selectedNavItem(location) {
        return createSelector([NavigationState], (state: NavigationStateModel) => {

            return state.menus[location][state.selected_nav];
        })
    }

    @Selector()
    static getMenuItems(location) {
        return createSelector([NavigationState], (state: NavigationStateModel) => {
            return state['navigation'].menus[location].menuItems;
        })
    }

    @Action(navActions.FetchNavItems)
    fetchNavItems(ctx: StateContext<NavigationStateModel>, action: navActions.FetchNavItems) {
        return this._coreSvc.getAllNavigationMenues().pipe(
            tap((data) => {
                ctx.patchState({ loading: true, loaded: false });
                ctx.dispatch(new navActions.FetchNavItemsSuccess(this.MapToAppMenus(data)));
            })
        );
    }

    @Action(navActions.FetchNavItemsSuccess)
    fetchNavItemsSuccess(ctx: StateContext<NavigationStateModel>, action: navActions.FetchNavItemsSuccess) {
        const payload = action.payload;

        const url = this.router.routerState.snapshot.url;

        if (url === "/" || url === '/home') {
            payload.find(m => m.locations[0] === 'PRIMARY').menuItems[0].isSelected = true;
        } else if (url.includes('/posts/')) {
            payload.find(m => m.locations[0] === 'PRIMARY').menuItems.find(n => n.path === `/posts/`).isSelected = true;
        }
        else if (url.includes('/tag/')) {
            payload.find(m => m.locations[0] === 'PRIMARY').menuItems.find(n => n.path === `/posts/`).isSelected = true;
        }
        else {
            payload.find(m => m.locations[0] === 'PRIMARY').menuItems.find(n => n.path.includes(`${url}/`)).isSelected = true;
        }
        ctx.patchState({
            loading: false,
            loaded: true,
            nav_loaded: true,
            menus: Utils.Helpers.ToHashMap(payload, ctx.getState(), 'locations'),
            selected_nav: payload.find(m => m.locations[0] === 'PRIMARY').menuItems.find(n => n.isSelected).id
        });
    }

    @Action(navActions.FetchNavItemsFail)
    fetchNavItemsFail(ctx: StateContext<NavigationStateModel>, action: navActions.FetchNavItemsFail) {
        ctx.patchState({
            loading: false,
            loaded: false,
        });
    }

    @Action(navActions.SetSelectedNavItem)
    setSelectedNavItem(ctx: StateContext<NavigationStateModel>, action: navActions.SetSelectedNavItem) {
        const state = ctx.getState();
        if (state.menus) {
            const navItems = Utils.Helpers.FromHashMap<AppRoute>(state.menus['PRIMARY'].menuItems);
            let navItem = navItems.find((r: AppRoute) => r.path === action.payload) || navItems.find((r: AppRoute) => r.path.includes(action.payload));
            if (navItem) {

                ctx.patchState({
                    selected_nav_loading: true,
                    selected_nav_loaded: false
                });

                ctx.dispatch(new navActions.SetSelectedNavItemSuccess(navItem.id));
            }
        }
    }

    @Action(navActions.SetSelectedNavItemSuccess)
    setSelectedNavItemSuccess(ctx: StateContext<NavigationStateModel>, action: navActions.SetSelectedNavItemSuccess) {
        const payload = action.payload;

        ctx.patchState({
            selected_nav_loading: false,
            selected_nav_loaded: true,
            selected_nav: payload,
        });
    }

    @Action(navActions.SetSelectedNavItemFail)
    setSelectedNavItemFail(ctx: StateContext<NavigationStateModel>, action: navActions.SetSelectedNavItemFail) {
        ctx.patchState({
            selected_nav_loading: false,
            selected_nav_loaded: false,
        });
    }

    @Action(navActions.UpdateURL)
    updateURL(ctx: StateContext<NavigationStateModel>, action: navActions.UpdateURL) {
        const state = ctx.getState();
        const url: string = action.payload;

        ctx.patchState({
            previous_url: state.current_url,
            current_url: url,
        });
    }

    private MapToAppRoutes(data: menuQuery): AppRoute[] {
        return data.edges.map((i) => {
            return {
                id: i.node.id,
                path: i.node.path,
                label: i.node.label,
                isSelected: false
            }
        })
    }

    private MapToAppMenus(data: MenusQuery): AppMenu[] {
        return data.menus.edges.map(
            (m) => {
                const menu: any = { ...m.node };

                menu.menuItems = this.MapToAppRoutes(m.node.menuItems)

                return menu as AppMenu;
            }
        )
    }
}