// the main types of pages
export type PageType  = 'page' | 'category' | 'tag' | 'posts' | 'post' | 'reviews';