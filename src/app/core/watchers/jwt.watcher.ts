
import { Injectable } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { UsersState } from 'src/app/store/state/users.state';
import { DecodedJWT } from '../interfaces/jwt.interface';
import jwt_decode from "jwt-decode";
import { AppState } from '../store/state/app.store';
import { RefreshToken } from '../store/actions/auth.actions';

@Injectable({
    providedIn: 'root'
})
export class JWTWatcher {
    @Select(AppState.Token) token$: Observable<string>;
    private _decoded_token: DecodedJWT;
    private timer: number;
    private minutes: number = 5*60000;

    constructor(private store: Store) {
        this.token$.subscribe({
            next: res => { if (res) this.initWatcher(res) }
        })
    }

    private initWatcher(token: string) {
        const now: number = Date.now() - this.minutes;

        this._decoded_token = this.decodeToken(token);

        if (now >= new Date(this._decoded_token.exp*1000).getTime()) this.fetchNewToken(token);
        else {
            this.timer = new Date(this._decoded_token.exp).getTime() - now;
            setTimeout(() => {
                this.fetchNewToken(token);
            }, this.timer);
        }

    }

    private fetchNewToken(token) {
        this.store.dispatch(new RefreshToken(token))
    }

    private decodeToken(token) {
        return jwt_decode<DecodedJWT>(token)
    }

}