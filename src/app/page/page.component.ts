import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { AppState } from '../core/store/state/app.store';
import { TemplateEngineState } from '../store/state/template-engine.store';

@Component({
  selector: 'blog-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  @Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
  @Select(TemplateEngineState.getSelectedPage) selectdPage$: Observable<any>;
  @Select(TemplateEngineState.isLoaded) appLoaded$: Observable<boolean>;

  constructor(private store: Store, private router: Router, private route: ActivatedRoute) {}

  ngOnInit(): void {

  }

}
