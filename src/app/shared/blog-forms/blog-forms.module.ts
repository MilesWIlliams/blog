import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormInputComponent } from './partials/form-input/form-input.component';
import { FormComponent } from './component/form.component';
import { DynamicFormDirective } from './directives/dynamic-form.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { InputElementNumberComponent } from './partials/input-element-number/form-input-number.component';
import { SliderComponent } from './partials/slider/slider.component';

@NgModule({
	declarations: [
		FormInputComponent,
		FormComponent,
		DynamicFormDirective,
		InputElementNumberComponent,
		SliderComponent
	],
	imports: [
		CommonModule,
		ReactiveFormsModule
	],
	entryComponents: [
		FormInputComponent,
		InputElementNumberComponent,
		SliderComponent
	],
	exports: [
		FormComponent,
		InputElementNumberComponent,
		SliderComponent
	]
})
export class BlogFormsModule { }
