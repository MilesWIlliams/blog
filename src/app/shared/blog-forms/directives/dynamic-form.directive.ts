import { ComponentFactoryResolver, ComponentRef, Directive, Input, OnChanges, OnInit, Type, ViewContainerRef } from '@angular/core';
import { FormGroup } from '@angular/forms';



import { Field } from '../interfaces/field.interface';
import { FieldConfig } from '../interfaces/field-config.interface';
import { FormInputComponent } from '../partials/form-input/form-input.component';
import { InputElementNumberComponent } from '../partials/input-element-number/form-input-number.component';
import { SliderComponent } from '../partials/slider/slider.component';

const components: { [type: string]: Type<Field> } = {
	input: FormInputComponent,
	input_number: InputElementNumberComponent,
	slider: SliderComponent
};

@Directive({
	selector: '[dynamicForm]'
})
export class DynamicFormDirective implements Field, OnChanges, OnInit {
	@Input()
	config: FieldConfig;

	@Input()
	group: FormGroup;

	component: ComponentRef<Field>;

	constructor(
		private resolver: ComponentFactoryResolver,
		private container: ViewContainerRef
	) { }

	ngOnChanges() {
		if (this.component) {
			this.component.instance.config = this.config;
			this.component.instance.group = this.group;
		}
	}

	ngOnInit() {
		if (!components[this.config.type]) {
			const supportedTypes = Object.keys(components).join(', ');
			throw new Error(
				`Trying to use an unsupported type (${this.config.type}).
				Supported types: ${supportedTypes}`
			);
		}
		const component = this.resolver.resolveComponentFactory<Field>(components[this.config.type]);
		this.component = this.container.createComponent(component);
		this.component.instance.config = this.config;
		this.component.instance.group = this.group;
	}
}
