import { ValidatorFn } from '@angular/forms';
import { FieldOptions } from './field-options.interfac';

export interface FieldConfig {
    disabled?: boolean,
    label?: string,
    name: string,
    options?: FieldOptions,
    placeholder?: string,
    type: 'input' | 'button' | 'select' | 'input_number' | 'slider',
    input_type?: 'text' | 'number' | 'currency' | 'email' | 'password' | 'range',
    validation?: ValidatorFn[],
    value?: any
}
