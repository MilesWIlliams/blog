export interface FieldOptions {
    step?: number,
    min?: number,
    max?: number,
}