import { Component, OnInit } from '@angular/core';
import { Field } from '../../interfaces/field.interface';
import { FieldConfig } from '../../interfaces/field-config.interface';
import { FormGroup } from '@angular/forms';

@Component({
	selector: 'blog-form-input',
	templateUrl: './form-input.component.html',
	styleUrls: ['./form-input.component.scss']
})
export class FormInputComponent implements Field {
	config: FieldConfig;
	group: FormGroup;

	constructor() { }


}
