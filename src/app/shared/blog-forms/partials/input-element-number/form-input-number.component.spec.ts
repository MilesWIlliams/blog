import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputElementNumberComponent } from './form-input-number.component';

describe('InputElementNumberComponent', () => {
  let component: InputElementNumberComponent;
  let fixture: ComponentFixture<InputElementNumberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InputElementNumberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputElementNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
