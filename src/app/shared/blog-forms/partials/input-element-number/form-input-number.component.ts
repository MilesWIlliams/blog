import { Component, HostListener, Input,OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from '../../interfaces/field-config.interface';
import { Field } from '../../interfaces/field.interface';

@Component({
	selector: 'blog-input-element-number',
	templateUrl: './form-input-number.component.html',
	styleUrls: ['./form-input-number.component.scss']
})
export class InputElementNumberComponent implements Field,  OnInit {
	@Input() label: string;
	config: FieldConfig;
	group: FormGroup;
	public _step: number = 1;

	constructor() { }

	ngOnInit() {
		if (this.config && this.config.options && this.config.options.step) {
			this._step = this.config.options.step;
		}
	}

	public increase() {
		if(this.getControl()) {
			const current_value = Number(this.getControl().value);
			const new_value = Number(current_value + this._step).toFixed(2);

			this.getControl().patchValue(new_value);
		}
	}

	public decrease() {
		if(this.getControl()) {
			const current_value = Number(this.getControl().value);
			const new_value = Number(current_value - this._step).toFixed(2);

			this.getControl().patchValue(new_value);
		}
	}

	private getControl() {
		return this.group.get(this.config.name);
	}

	@HostListener('keyup', ['$event'])
	onKeyUp(e) {
		const srcEl = e.target as HTMLElement;

		if(srcEl.id === this.config.name) {
			switch (e.code) {
				case 'ArrowUp':
					this.increase()
					break;
				case 'ArrowDown':
					this.decrease()
					break;
			}
		}
	}

}
