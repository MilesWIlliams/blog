import { DOCUMENT } from '@angular/common';
import { AfterContentChecked, AfterContentInit, Component, Inject, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FieldConfig } from '../../interfaces/field-config.interface';
import { Field } from '../../interfaces/field.interface';

@Component({
  selector: 'blog-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements Field, OnInit, AfterContentChecked {
  @Input() label: string;
  config: FieldConfig;
  group: FormGroup;
  va
  constructor(@Inject(DOCUMENT) private dom: Document) { }

  ngOnInit(): void {

  }

  ngAfterContentChecked() {
    const inputEl = this.dom.getElementById(this.config.name) as HTMLInputElement;
    const bubble = this.dom.querySelector(".bubble");
    // console.log('inputEl', inputEl);

    if (inputEl) {
      //   inputEl.addEventListener('change',() => {
      //     var value = (Number(inputEl.value) -  Number(inputEl.min))/(Number(inputEl.max)- Number(inputEl.min))*100
      //     console.log('val', value);

      //     inputEl.style.background = 'linear-gradient(to right, #82CFD0 0%, #82CFD0 ' + value + '%, #fff ' + value + '%, white 100%)'
      inputEl.addEventListener("input", () => {
        this.setBubble(inputEl, bubble);
      });
      this.setBubble(inputEl, bubble);
    // })
    }
  }

  setBubble(range, bubble) {
    const val = range.value;
    const min = range.min ? range.min : 0;
    const max = range.max ? range.max : 100;
    const newVal = Number(((val - min) * 100) / (max - min));
    bubble.innerHTML = val;

    // Sorta magic numbers based on size of the native UI thumb
    bubble.style.left = `calc(${newVal}% + (${8 - newVal * 0.15}px))`;
  }

}
