import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'blog-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
  @Input() isType:  'submit' | 'cancel' = 'submit';
  @Input() isClass: 'primary' | 'alt' | 'solid' = 'primary';
  @Input() isSize:  'xLarge' | 'large' | 'medium' | 'small' |'smallest' = 'large';
  @Input() isDisabled: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

}
