import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SlIconComponent } from './elements/sl-icon/sl-icon.component';
import { ListWidgetComponent } from './elements/list-widget/list-widget.component';
import { ButtonComponent } from './elements/button/button.component';
import { PageSectionComponent } from './templates/partials/page-section/page-section.component';
import { ModalComponent } from './elements/modal/modal.component';
import { RouterModule } from '@angular/router';
import { FrontPageComponent } from './templates/pages/front-page/front-page.component';
import { AdBannerComponent } from './templates/partials/ad-banner/ad-banner.component';
import { ArchivePageComponent } from './templates/pages/archive-page/archive-page.component';
import { PageHeaderComponent } from './templates/partials/page-header/page-header.component';
import { PostSectionComponent } from './templates/partials/post-section/post-section.component';
import { WidgetTitleBarComponent } from './templates/partials/widget-title-bar/widget-title-bar.component';
import { CategoryFeaturedPostBlockComponent } from './templates/partials/category-featured-post-block/category-featured-post-block.component';
import { UtilsModule } from '../utils/utils.module';
import { SidebarComponent } from './templates/partials/sidebar/sidebar.component';
import { ArchiveSingleComponent } from './templates/pages/archive-single/archive-single.component';
import { PostBlockComponent } from './templates/partials/post-block/post-block.component';
import { PostDetailsComponent } from './templates/partials/post-details/post-details.component';
import { SimilarPostsComponent } from './templates/partials/similar-posts/similar-posts.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { PagininationComponent } from './templates/partials/paginination/paginination.component';
import { TagsComponent } from './templates/partials/tags/tags.component';
import { BannerComponent } from './templates/partials/banner/banner.component';
import { CalculatorsPageComponent } from './templates/pages/calculators-page/calculators-page.component';
import { ReviewsPageComponent } from './templates/pages/reviews-page/reviews-page.component';
import { AuthorBioComponent } from './templates/partials/author-bio/author-bio.component';
import { CommentsComponent } from './templates/partials/comments/comments.component';
import { UserMenuComponent } from './templates/partials/user-menu/user-menu.component';
import { SiteSearchbarComponent } from './templates/partials/site-searchbar/site-searchbar.component';
import { AuthPageComponent } from './templates/pages/auth-page/auth-page.component';
import { BlogFormsModule } from './blog-forms/blog-forms.module';
import { LatestPostsComponent } from './templates/partials/latest-posts/latest-posts.component';
import { FeaturedPostsComponent } from './templates/partials/featured-posts/featured-posts.component';
import { FooterComponent } from './templates/partials/footer/footer.component';
import { SearchPageComponent } from './templates/pages/search-page/search-page.component';
import { ReviewPostComponent } from './templates/partials/review-post/review-post.component';
import { ReviewRatingsComponent } from './templates/partials/review-ratings/review-ratings.component';
import { LabelComponent } from './templates/partials/label/label.component';
import { ReviewSingleComponent } from './templates/pages/review-single/review-single.component';
import { DownloadsPageComponent } from './templates/pages/downloads-page/downloads-page.component';
import { SuggestPageComponent } from './templates/pages/suggest-page/suggest-page.component';
import { TabsModule } from './templates/partials/tabs/tabs.module';
import { RatingsModule } from './templates/partials/review-ratings/ratings.module';



@NgModule({
  declarations: [
    SlIconComponent,
    ListWidgetComponent,
    ButtonComponent,
    PageSectionComponent,
    ModalComponent,
    FrontPageComponent,
    AdBannerComponent,
    ArchivePageComponent,
    PageHeaderComponent,
    PostSectionComponent,
    WidgetTitleBarComponent,
    CategoryFeaturedPostBlockComponent,
    SidebarComponent,
    ArchiveSingleComponent,
    PostBlockComponent,
    PostDetailsComponent,
    SimilarPostsComponent,
    PagininationComponent,
    TagsComponent,
    BannerComponent,
    ReviewsPageComponent,
    CalculatorsPageComponent,
    AuthorBioComponent,
    CommentsComponent,
    UserMenuComponent,
    SiteSearchbarComponent,
    AuthPageComponent,
    LatestPostsComponent,
    FeaturedPostsComponent,
    FooterComponent,
    SearchPageComponent,
    ReviewPostComponent,
    LabelComponent,
    ReviewSingleComponent,
    DownloadsPageComponent,
    SuggestPageComponent,

  ],
  exports: [
    SlIconComponent,
    ListWidgetComponent,
    ButtonComponent,
    PageSectionComponent,
    ModalComponent,
    AdBannerComponent,
    FrontPageComponent,
    ArchivePageComponent,
    CategoryFeaturedPostBlockComponent,
    ArchiveSingleComponent,
    SimilarPostsComponent,
    BannerComponent,
    ReviewsPageComponent,
    CalculatorsPageComponent,
    AuthorBioComponent,
    UserMenuComponent,
    SiteSearchbarComponent,
    AuthPageComponent,
    BlogFormsModule,
    FooterComponent,
    SearchPageComponent,
    ReviewSingleComponent,
    DownloadsPageComponent,
    SuggestPageComponent,
    TabsModule,
    RatingsModule
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    UtilsModule,
    CarouselModule,
    BlogFormsModule,
    TabsModule,
    RatingsModule
  ]
})
export class SharedModule { }
