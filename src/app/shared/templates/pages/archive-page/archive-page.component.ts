import { Component, Input, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Post } from 'src/app/core/interfaces/post.interface';
import { PostState } from 'src/app/store/state/post.store';

import { Page } from 'src/app/utils/models/page.model';

@Component({
  selector: 'blog-archive-page',
  templateUrl: './archive-page.component.html',
  styleUrls: ['./archive-page.component.scss']
})
export class ArchivePageComponent implements OnInit {
  @Input() page: Page;
  @Select(PostState.getAllPosts) posts$: Observable<Post[]>;

  constructor() { }

  ngOnInit(): void {
    window.scrollTo({top: 0});
  }

}
