import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveSingleComponent } from './archive-single.component';

describe('ArchiveSingleComponent', () => {
  let component: ArchiveSingleComponent;
  let fixture: ComponentFixture<ArchiveSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArchiveSingleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
