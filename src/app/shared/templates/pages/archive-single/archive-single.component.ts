import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Paginination } from 'src/app/core/interfaces/pagination.interface';
import { Post } from 'src/app/core/interfaces/post.interface';
import { PostState } from 'src/app/store/state/post.store';
import { Page } from 'src/app/utils/models/page.model';

@Component({
  selector: 'blog-archive-single',
  templateUrl: './archive-single.component.html',
  styleUrls: ['./archive-single.component.scss']
})
export class ArchiveSingleComponent implements OnInit, OnChanges {
  @Input() page: Page;
  @Select(PostState.getSelectedPost) post$: Observable<Post>;
  constructor() {
   }

  ngOnInit(): void {
  }

  public buildPaginationUrls(post: Post): Paginination {
    const prevous_slug = post.previous?.slug ? `/posts/${post.previous.slug}` : '';
    const next_slug = post.next?.slug ? `/posts/${post.next.slug}` : '';
    return {
      previous_post_url: prevous_slug,
      next_post_url: next_slug
    }
  }

  ngOnChanges() {
    window.scrollTo({top: 0});
  }

}
