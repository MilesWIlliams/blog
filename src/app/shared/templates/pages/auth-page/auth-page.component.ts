import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { LoginUserPayload, RegisterUserPayload } from 'src/app/core/interfaces/app-user.interface';
import { BlogPage } from 'src/app/core/interfaces/page.interface';
import { AuthenticateUser, RegisterUser } from 'src/app/core/store/actions/auth.actions';
import { FieldConfig } from 'src/app/shared/blog-forms/interfaces/field-config.interface';
import { TemplateEngineState } from 'src/app/store/state/template-engine.store';

@Component({
	selector: 'blog-auth-page',
	templateUrl: './auth-page.component.html',
	styleUrls: ['./auth-page.component.scss']
})
export class AuthPageComponent implements OnInit, OnDestroy {
	@Select(TemplateEngineState.getSelectedPage) selectdPage$: Observable<BlogPage>;
	public page: BlogPage;
	private ngDestroy: Subject<Boolean> = new Subject();
	public formConfig: FieldConfig[];

	constructor(private fb: FormBuilder, private store: Store) { }

	public authForm: FormGroup;
	public buttonText: string;
	public signupPasswordchecks = [
		{
			text: 'One uppercase letter',
			passed: false,
			test: 'uppercase'
		},
		{
			text: 'One numeral number',
			passed: false,
			test: 'numerical'
		},
		{
			text: 'One special character',
			passed: false,
			test: 'special_char'
		},
		{
			text: 'Eight character minimum',
			passed: false,
			test: 'length'
		}
	];

	ngOnInit(): void {
		this.selectdPage$.subscribe({
			next: (page) => { 
				this.page = page;
				this.initForm(page);
			}
		})
	}

	private initForm(page:BlogPage) {
		switch (page.slug) {
			case 'log-in':
				this.authForm = this.fb.group({
					username: this.fb.control('', Validators.compose([Validators.required])),
					password: this.fb.control('',
					Validators.compose(
						[
							Validators.minLength(8),
							Validators.required,
							Validators.email,
						]
					)),
				});
				this.buttonText = "log in";
				this.formConfig = [
					{
						label: 'Username',
						name: 'username',
						placeholder: 'Type here...',
						type: 'input',
						input_type: 'text',
					},
					{
						label: 'Password',
						name: 'password',
						placeholder: 'Type here...',
						type: 'input',
						input_type: 'password',
					}
				];
				break;

			case 'sign-up':
				this.authForm = this.fb.group({
					username: this.fb.control('', Validators.required),
					emailAddress: this.fb.control('', Validators.compose([
						Validators.required,
						Validators.email
					])),
					password: this.fb.control('', 
					Validators.compose(
						[
							Validators.minLength(8),
							Validators.required,
							Validators.email,
						]
					)),
					// terms: this.fb.control(false, Validators.requiredTrue)
				});
				this.buttonText = "Open and account";
				this.formConfig = [
					{
						label: 'Username',
						name: 'username',
						placeholder: 'Type here...',
						type: 'input',
						input_type: 'text',
					},
					{
						label: 'Email Address',
						name: 'emailAddress',
						placeholder: 'Type here...',
						type: 'input',
						input_type: 'email',
					},
					{
						label: 'Password',
						name: 'password',
						placeholder: 'Type here...',
						type: 'input',
						input_type: 'password',
						validation: []
					},
				];
				break;
		}
	}

	public receiveFormChanges({control, value}) {
		this.authForm.get(control).patchValue(value);
		this.authForm.get(control).markAsDirty();
		if(control === 'password') {
			this.evaluatePasswordChcks(value)
		}
	}

	private evaluatePasswordChcks(password: string) {

		//check uppercase
		this.signupPasswordchecks.find(c => c.test === 'uppercase').passed = new RegExp('^(?=.*[A-Z]).+$').test(password);
		//checker num
		this.signupPasswordchecks.find(c => c.test === 'numerical').passed = new RegExp('^(?=.*\\d).+$').test(password);
		//check special character
		this.signupPasswordchecks.find(c => c.test === 'special_char').passed = new RegExp('^(?=.*[-+_!@#$%^&*., ?]).+$').test(password);
		//check 8 character minumn
		this.signupPasswordchecks.find(c => c.test === 'length').passed = password.length >= 8;
	}

	public registerSignInUser() {
		if (this.authForm.valid) {
			if (this.page.slug ==='sign-up') {
				const payload: RegisterUserPayload = {
					username: this.authForm.get('username').value,
					email: this.authForm.get('emailAddress').value,
					password: this.authForm.get('password').value
				}
				this.store.dispatch(new RegisterUser(payload));
			} else {
				const payload: LoginUserPayload = {
					username: this.authForm.get('username').value,
					password: this.authForm.get('password').value
				}

				this.store.dispatch(new AuthenticateUser(payload));
			}
			this.authForm.reset();
		}
	}

	ngOnDestroy() {
		this.ngDestroy.next(null);
		this.ngDestroy.complete();
	}

}
