import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { AppState } from 'src/app/core/store/state/app.store';
import { FieldConfig } from 'src/app/shared/blog-forms/interfaces/field-config.interface';
import { Page } from 'src/app/utils/models/page.model';

@Component({
	selector: 'blog-calculators-page',
	templateUrl: './calculators-page.component.html',
	styleUrls: ['./calculators-page.component.scss']
})
export class CalculatorsPageComponent implements OnInit {
	@Input() page: Page;
	@Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
	private ngDestroy: Subject<Boolean> = new Subject();
	public mortgageConfig: FieldConfig[];
	public mortgageForm: FormGroup;
	public mortgageResultsForm: FormGroup;
	public rentalForm: FormGroup;


	constructor(private fb: FormBuilder) {
		this.mortgageForm = this.fb.group({
			purchase_price: this.fb.control(' - ', Validators.required),
			deposit: this.fb.control(' - '),
			mortgage_term: this.fb.control(' - ', Validators.required),
			interest: this.fb.control(' - ', Validators.required),
			interest_slider: this.fb.control(' - '),
		});

		this.mortgageResultsForm = this.fb.group({
			monthly: this.fb.group({
				interest: this.fb.control(''),
				principal: this.fb.control(''),
				total: this.fb.control(''),
			}),
			yearly: this.fb.group({
				interest: this.fb.control(''),
				principal: this.fb.control(''),
				total: this.fb.control(''),
			}),
			total: this.fb.group({
				interest: this.fb.control(''),
				principal: this.fb.control(''),
				total: this.fb.control(''),
			})
		});

		this.rentalForm = this.fb.group({
			purchase_price: this.fb.control('0.00', Validators.required),
			deposit: this.fb.control('0.00', Validators.required),
			mortgage_term: this.fb.control('72', Validators.required),
			interest: this.fb.control('72', Validators.required),
			transfer_costs: this.fb.control('0.00', Validators.required),
			repairs: this.fb.control('0.00'),
			operating_costs: this.fb.control('72', Validators.required),
			rental_amount: this.fb.control('72', Validators.required),
		});

		this.mortgageConfig = [
			{
				label: 'Purchase Price',
				name: 'purchase_price',
				placeholder: 'Amount...',
				type: 'input_number',
				input_type: 'number',
				value: '0.00',
				options: {
					step: 0.01
				}
			},
			{
				label: 'Deposit',
				name: 'deposit',
				placeholder: 'Amount...',
				type: 'input_number',
				input_type: 'number',
				value: '0.00',
				options: {
					step: 0.01
				}
			},
			{
				label: 'Mortgage term ( y )',
				name: 'mortgage_term',
				placeholder: 'Amount...',
				type: 'input_number',
				input_type: 'number',
				value: '0.00',
				options: {
					step: 0.01
				}
			},
			{
				label: 'Interest',
				name: 'interest',
				placeholder: 'Amount...',
				type: 'input_number',
				input_type: 'number',
				value: '0.00',
				options: {
					step: 0.01
				}
			},
			{
				label: 'Interest',
				name: 'interest_slider',
				placeholder: 'Amount...',
				type: 'slider',
				input_type: 'range',
				value: '0.00',
				options: {
					step: 0.01,
					min: 0,
					max: 30
				}
			},
		];
	}

	ngOnInit(): void {
	}
	public receiveFormChanges({ control, value }) {
		this.mortgageForm.get(control).patchValue(value);
		// this.mortgageForm.get(control).markAsDirty();

			if (this.mortgageForm.valid) {
				const ir = Number(this.mortgageForm.get('interest').value )/ 1200;
				const period = Number(this.mortgageForm.get('mortgage_term').value) *12;
				const loanAmount = Number(this.mortgageForm.get('purchase_price').value)
				this.calculateMonthlyPayments(ir, period, loanAmount);
			}
	}

	private calculateMonthlyPayments(rate, periods, pv) {
		const monthlyPMT = this.PMT(rate, periods, pv);

		const monthlyInterest = pv * (rate);
		const yearlyPMT = monthlyPMT * 12;
		const yearlyInterest = monthlyInterest * 12;


		this.mortgageResultsForm.get('monthly').get('total').patchValue(monthlyPMT);
		this.mortgageResultsForm.get('monthly').get('interest').patchValue(monthlyInterest);
		this.mortgageResultsForm.get('monthly').get('principal').patchValue(monthlyPMT- monthlyInterest);

		this.mortgageResultsForm.get('yearly').get('total').patchValue(yearlyPMT);
		this.mortgageResultsForm.get('yearly').get('interest').patchValue(yearlyInterest);
		this.mortgageResultsForm.get('yearly').get('principal').patchValue(yearlyPMT- yearlyInterest);

		this.mortgageResultsForm.get('total').get('total').patchValue(yearlyPMT * (periods / 12));
		this.mortgageResultsForm.get('total').get('interest').patchValue(yearlyInterest * (periods / 12));
		this.mortgageResultsForm.get('total').get('principal').patchValue(( yearlyPMT * (periods / 12)) - (yearlyInterest * (periods / 12)));
	}

	PMT(rate, periods, present, future = 0, type = 0) {

		if (rate === 0) {
			return -((present + future) / periods);
		} else {
			var term = Math.pow(1 + rate, periods);
			if (type === 1) {
				return ((future * rate / (term - 1) + present * rate / (1 - 1 / term)) / (1 + rate));
			} else {
				return (future * rate / (term - 1) + present * rate / (1 - 1 / term));
			}
		}

	};

}
