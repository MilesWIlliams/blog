import { Component, Input, OnInit } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Category } from 'src/app/core/interfaces/category.interface';
import { FrontPagePostCategory, FrontPagePostGroupSection } from 'src/app/core/interfaces/post.interface';
import { AppState } from 'src/app/core/store/state/app.store';
import { Page } from 'src/app/utils/models/page.model';

@Component({
  selector: 'blog-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.scss']
})
export class FrontPageComponent implements OnInit {
  @Input() page: Page;
  @Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;

  constructor(private  store: Store) { }

  ngOnInit(): void {
  }

  public loopPostSections(object: FrontPagePostGroupSection) {

    if (object.categoryPosts && Object.keys(object.categoryPosts).length > 1) {
      const sections =  Object.keys(object.categoryPosts).map((k) =>  {
        if (k.includes('Section')) {
          return object.categoryPosts[k]
        }
      });
  
      const filtered = sections.filter(x => x !== undefined);
  
      return filtered;
    }
  }

  public goToCategory(section: FrontPagePostCategory) {
    const category = section.posts[0].categories.nodes[0];
    return `/category/${category.slug}`;
  }

}
