import { Component, Inject, OnInit, Optional } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { VIEWER } from 'src/app/core/config/app.config';
import { Image } from 'src/app/core/interfaces/image.interface';
import { CompanyDetails, CompanyDetailsDataObject, Review } from 'src/app/core/interfaces/review.interface';
import { AppState } from 'src/app/core/store/state/app.store';
import { ReviewState } from 'src/app/store/state/reviews.state';
import { Utils } from 'src/app/utils';

@Component({
	selector: 'blog-review-single',
	templateUrl: './review-single.component.html',
	styleUrls: ['./review-single.component.scss']
})
export class ReviewSingleComponent implements OnInit {
	@Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
	@Select(ReviewState.getSelectedReview) review$: Observable<Review>;
	public isBot: boolean;

	constructor(private store: Store, @Optional() @Inject(VIEWER) private viewer) {
		this.isBot = this.viewer === 'bot';
	}
	ngOnInit(): void {
	}

	public buildImgArray(media: { [prop: string]: Image }): Image[] {
		return Utils.Helpers.FromHashMap<Image>(media).filter(i => i != null);
	}

	public buildCompanyDetails(detailsObject: CompanyDetailsDataObject): any[] {
		const fields: Array<{
			class: string,
			label: string,
			value,
			position: number
		}> = [];

		Object.keys(detailsObject).forEach(
			(k) => {
				const value = detailsObject[k];
				switch (k) {
					case 'productPricingDetails':
						if (value.price != null) {
							fields.push({
								class: 'fee',
								label:  "Fee's",
								value: `$ ${Number(value.price).toFixed(2)}`,
								position: 3
							});
						}
						break;
					case 'startDate':
						if (value != null) {
							fields.push({
								class: 'date',
								label:  "Start date",
								value: value,
								position: 2

							});
						}
						break;
					case 'websiteUrl':
						if (value != null) {
							fields.push({
								class: 'website',
								label:  "Website",
								value: value,
								position: 1
							});
						}
						break;
					case 'promotionsDetails':
						if (value.promotionsLinkText != null) {
							fields.push({
								class: 'promotion',
								label:  "Promotions",
								value: value.promotionsLinkText,
								position: 4
							});
						}
						break;
					default:
						break;
				}
			});

		return fields.sort((a,b) => (a.position > b.position) ? 1 : ((b.position > a.position) ? -1 : 0));
	}

	public formatDate(date: string) {
		const dateArr = date.split('/');
		const fDate = `${dateArr[1]}/${dateArr[0]}/${dateArr[2]}`
		return new Date(fDate);
	}

	public getKeys(o:Object) {
		return Object.keys(o);
	}
}
