import { Inject, Optional } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { VIEWER } from 'src/app/core/config/app.config';
import { Review } from 'src/app/core/interfaces/review.interface';
import { FetchReviews } from 'src/app/store/actions/reviews.actions';
import { ReviewState } from 'src/app/store/state/reviews.state';
import { Page } from 'src/app/utils/models/page.model';

@Component({
	selector: 'blog-reviews-page',
	templateUrl: './reviews-page.component.html',
	styleUrls: ['./reviews-page.component.scss']
})
export class ReviewsPageComponent implements OnInit {
	@Input() page: Page;
	@Select(ReviewState.getReviews)  reviews$: Observable<Review[]>;
	public isBot: boolean;

	constructor(private store: Store, @Optional() @Inject(VIEWER) private viewer) {
		this.store.dispatch(new FetchReviews());
		this.isBot = this.viewer === 'bot';
	}

	ngOnInit(): void {
	}

}
