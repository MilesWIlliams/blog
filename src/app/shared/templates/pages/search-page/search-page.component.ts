import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Post } from 'src/app/core/interfaces/post.interface';
import { SearchPosts } from 'src/app/store/actions/search.actions';
import { SearchState } from 'src/app/store/state/search.state';

@Component({
	selector: 'blog-search-page',
	templateUrl: './search-page.component.html',
	styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {
	@Select(SearchState.getSearchResults) searchResults$: Observable<Post[]>;
	@Select(SearchState.getQueryString) query$: Observable<string>;
	@ViewChild('searchInput') searchInput: ElementRef;
	public query: string = this.store.selectSnapshot(SearchState.getQueryString);
	public searchControl: FormControl;

	constructor(private store: Store, private fb: FormBuilder) { 
		this.searchControl = this.fb.control(this.query, Validators.required);
	}

	ngOnInit(): void {
	}

	ngAfterViewInit() {
		if (this.searchInput) {
			this.searchInput.nativeElement.addEventListener('keyup', (e) => {
				if (e.key === 'Enter') this.onSearch()
			})
		}
	}

	private onSearch() {
		const query: string = this.searchControl.value;
		if (query.length > 0) {
			this.store.dispatch(new SearchPosts({ query: query }))
		}
	}

}
