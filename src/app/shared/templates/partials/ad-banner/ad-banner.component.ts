import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Optional } from '@angular/core';
import { Select } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable } from 'rxjs';
import { VIEWER } from 'src/app/core/config/app.config';
import { AdBanner } from 'src/app/core/interfaces/ad-banner.interface';
import { AppState } from 'src/app/core/store/state/app.store';
import { AdBannersState } from 'src/app/store/state/ad-banner.state';

@Component({
	selector: 'blog-ad-banner',
	templateUrl: './ad-banner.component.html',
	styleUrls: ['./ad-banner.component.scss']
})
export class AdBannerComponent implements OnInit {
	@Select(AdBannersState.getAllAdBanners) banners$: Observable<AdBanner[]>;
	@Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
	public isBot: boolean;
	private prevControl: HTMLElement;
	private nextControl: HTMLElement;

	customOptions: OwlOptions = {
		autoplay: true,
		autoplaySpeed: 1000,
		startPosition: 1,
		loop: true,
		mouseDrag: true,
		touchDrag: true,
		pullDrag: true,
		dots: false,
		navSpeed: 1000,
		responsive: {
			0: {
				items: 1,
			}
		},
		// nav: true,
	};
	constructor(@Inject(DOCUMENT) private dom: Document, @Optional() @Inject(VIEWER) private viewer) {
		this.isBot = this.viewer === 'bot';
	}

	ngOnInit(): void {
	}

	public onClickNextSlide() {
		this.nextControl = this.dom.querySelector('.owl-next');
		this.nextControl.click();
	}

	public onClickPrevSlide() {
		this.prevControl = this.dom.querySelector('.owl-prev');
		this.prevControl.click();
	}

	public sanitizeUrl(link: string) {
		const url = new URL(link);
		return `/posts/${url.pathname.split('/')[url.pathname.split('/').length -2]}`;
	}
}
