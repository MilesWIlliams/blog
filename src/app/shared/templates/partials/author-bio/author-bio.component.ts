import { Component, Input, OnInit } from '@angular/core';
import { Author } from 'src/app/core/interfaces/author.interface';

@Component({
	selector: 'blog-author-bio',
	templateUrl: './author-bio.component.html',
	styleUrls: ['./author-bio.component.scss']
})
export class AuthorBioComponent implements OnInit {
	@Input() authorBio: Author;
	constructor() { }

	ngOnInit(): void {
	}

}
