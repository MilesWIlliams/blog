import { Component, Input, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Post } from 'src/app/core/interfaces/post.interface';
import { PostState } from 'src/app/store/state/post.store';
import { Page } from 'src/app/utils/models/page.model';

@Component({
	selector: 'blog-banner',
	templateUrl: './banner.component.html',
	styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
	@Input() page: Page;
	@Select(PostState.getSelectedPost) selectedPost$: Observable<Post>;
	constructor() { }

	ngOnInit(): void {
	}

}
