import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryFeaturedPostBlockComponent } from './category-featured-post-block.component';

describe('CategoryFeaturedPostBlockComponent', () => {
  let component: CategoryFeaturedPostBlockComponent;
  let fixture: ComponentFixture<CategoryFeaturedPostBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryFeaturedPostBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryFeaturedPostBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
