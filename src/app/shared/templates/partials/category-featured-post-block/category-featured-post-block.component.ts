import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Post } from 'src/app/core/interfaces/post.interface';
import { AppState } from 'src/app/core/store/state/app.store';
import { ViewSinglePost } from 'src/app/store/actions/posts.actions';

@Component({
  selector: 'blog-category-featured-post-block',
  templateUrl: './category-featured-post-block.component.html',
  styleUrls: ['./category-featured-post-block.component.scss']
})
export class CategoryFeaturedPostBlockComponent implements OnInit {
  @Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
  @Input() layout: 'row' | 'column' = 'row';
  @Input() post: Post;

  constructor(private store: Store) { }

  ngOnInit(): void {

  }

  public goToPost(id: string) {
    return this.store.dispatch(new ViewSinglePost(id))
  }

}
