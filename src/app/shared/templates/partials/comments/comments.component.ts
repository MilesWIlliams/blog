import { Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/interfaces/author.interface';
import { PostCommentPayload } from 'src/app/core/interfaces/comment.interface';
import { Post } from 'src/app/core/interfaces/post.interface';
import { AppState } from 'src/app/core/store/state/app.store';
import { SubmitPostComment } from 'src/app/store/actions/posts.actions';
import { FetchUserByID } from 'src/app/store/actions/users.actions';
import { UsersState } from 'src/app/store/state/users.state';
import { Utils } from 'src/app/utils';

@Component({
  selector: 'blog-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  @Input() post: Post;
  @Select(UsersState.getAllUsersMapped) authors$: Observable<{[id: string]: User}>;
  // @Select(AppState.isAuthenticated) isLoggedIn$: Observable<boolean>;
  public isLogged: boolean = this.store.selectSnapshot(AppState.isAuthenticated);


  public commentForm: FormGroup;

  constructor(private store: Store, private fb: FormBuilder) { 

  }

  ngOnInit(): void {
    this.commentForm = this.fb.group({
      databaseId: this.fb.control(this.post.databaseId, Validators.required),
      author: this.fb.control('Miles Williams'),
      authorEmail: this.fb.control('miles.williams@gmail.com'),
      content: this.fb.control('', Validators.required)
    });
  }

  public sortComments(comments) {
    return Utils.Helpers.SortBy(comments, 'date');
  }

  public onSubmit() {
    let commentConfig: PostCommentPayload = this.commentForm.getRawValue();
    this.store.dispatch(new SubmitPostComment(commentConfig));
    this.commentForm.reset();
  }
}
