import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit, Optional } from '@angular/core';
import { Select } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable } from 'rxjs';
import { VIEWER } from 'src/app/core/config/app.config';

import { Post } from 'src/app/core/interfaces/post.interface';
import { AppState } from 'src/app/core/store/state/app.store';
import { PostState } from 'src/app/store/state/post.store';

@Component({
	selector: 'blog-featured-posts',
	templateUrl: './featured-posts.component.html',
	styleUrls: ['./featured-posts.component.scss']
})
export class FeaturedPostsComponent implements OnInit {
	@Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
	@Select(PostState.getFeaturedPosts) featuredPosts$: Observable<Post[]>
	public isBot: boolean;
	private prevControl: HTMLElement;
	private nextControl: HTMLElement;

	customOptions: OwlOptions = {
		// autoplay: true,
		autoplaySpeed: 700,
		startPosition: 0,
		loop: false,
		mouseDrag: true,
		touchDrag: true,
		pullDrag: true,
		dots: false,
		navSpeed: 700,
        responsive: {
            0: {
                items: 1,
            },
            400: {
                items: 2,
            },
            740: {
                items: 3,
            },
            940: {
                items: 4,
            },
        },
		nav: true,
	};
	constructor(@Inject(DOCUMENT) private dom: Document, @Optional() @Inject(VIEWER) private viewer) {
		this.isBot = this.viewer === 'bot';
	}
	ngOnInit(): void {
	}

	public onClickNextSlide() {
        this.nextControl = this.dom.querySelector('#featuredPosts .owl-next');
        this.nextControl.click();
    }

    public onClickPrevSlide() {
        this.prevControl = this.dom.querySelector('#featuredPosts .owl-prev');
        this.prevControl.click();
    }


}
