import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AppRoute } from 'src/app/core/interfaces/app-route.interface';
import { NavigationState } from 'src/app/core/store/state/navigation.store';

@Component({
  selector: 'blog-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Select(NavigationState.getMenuItems('FOOTER')) menuItems$: Observable<AppRoute[]>;

  constructor() { }

  ngOnInit(): void {
  }

  public getCopyrightDate(): string {
    const currentYear = new Date().getFullYear();

    return currentYear === 2021 ? `2021` : `2021 - ${currentYear}`;
  }

}
