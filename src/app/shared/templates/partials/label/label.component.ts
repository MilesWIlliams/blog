import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'blog-label',
	templateUrl: './label.component.html',
	styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {
	@Input() label: string;
	constructor() { }

	ngOnInit(): void {
	}

}
