import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'blog-latest-posts',
  templateUrl: './latest-posts.component.html',
  styleUrls: ['./latest-posts.component.scss']
})
export class LatestPostsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
