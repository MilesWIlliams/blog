import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'zk-page-section',
  templateUrl: './page-section.component.html',
  styleUrls: ['./page-section.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class PageSectionComponent implements OnInit {
  @Input() heading: string;
  @Input() layput: string
  constructor() { }

  ngOnInit(): void {
  }

}
