import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagininationComponent } from './paginination.component';

describe('PagininationComponent', () => {
  let component: PagininationComponent;
  let fixture: ComponentFixture<PagininationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagininationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagininationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
