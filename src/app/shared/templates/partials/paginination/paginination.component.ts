import { Component, Input, OnInit } from '@angular/core';
import { Paginination } from 'src/app/core/interfaces/pagination.interface';
import { PageInfoQuery } from 'src/app/types/post.type';

@Component({
  selector: 'blog-paginination',
  templateUrl: './paginination.component.html',
  styleUrls: ['./paginination.component.scss']
})
export class PagininationComponent implements OnInit {
  @Input() pageInfo: PageInfoQuery;
  @Input() pagination: Paginination
  constructor() { }

  ngOnInit(): void {
  }

}
