import { Component, Inject, Input, OnInit, Optional } from '@angular/core';
import { Store } from '@ngxs/store';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { VIEWER } from 'src/app/core/config/app.config';
import { Post } from 'src/app/core/interfaces/post.interface';
import { AppState } from 'src/app/core/store/state/app.store';
import { ViewSinglePost } from 'src/app/store/actions/posts.actions';

@Component({
  selector: 'blog-post-block',
  templateUrl: './post-block.component.html',
  styleUrls: ['./post-block.component.scss']
})
export class PostBlockComponent implements OnInit {
  @Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
  @Input() layout: 'row' | 'column' = 'row';
  @Input() post: Post;
	public isBot: boolean;

  constructor(private store: Store, @Optional() @Inject(VIEWER) private viewer) {
		this.isBot = this.viewer === 'bot';
  }

  ngOnInit(): void {
  }

  public goToPost(id: string) {
    return this.store.dispatch(new ViewSinglePost(id))
  }
}
