import { Component, Input, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Post } from 'src/app/core/interfaces/post.interface';

@Component({
  selector: 'blog-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit {
  @Input() post: Post;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public buildUrl() {
    return this.router.navigateByUrl(`/posts/${this.post.slug}?=comments`);
  }

}
