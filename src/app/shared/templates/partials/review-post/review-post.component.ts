import { Component, Input, OnInit } from '@angular/core';
import { Review } from 'src/app/core/interfaces/review.interface';

@Component({
	selector: 'blog-review-post',
	templateUrl: './review-post.component.html',
	styleUrls: ['./review-post.component.scss']
})
export class ReviewPostComponent implements OnInit {
	@Input() review: Review;
	@Input() isBot: boolean;
	constructor() { }

	ngOnInit(): void {
	}

}
