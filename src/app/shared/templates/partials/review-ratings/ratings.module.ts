import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewRatingsComponent } from './review-ratings.component';



@NgModule({
  declarations: [ReviewRatingsComponent],
  imports: [
    CommonModule
  ],
  exports:  [ReviewRatingsComponent]
})
export class RatingsModule { }
