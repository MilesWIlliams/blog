import { Component, OnInit, Input } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/core/store/state/app.store';

@Component({
	selector: 'blog-review-ratings',
	templateUrl: './review-ratings.component.html',
	styleUrls: ['./review-ratings.component.scss']
})
export class ReviewRatingsComponent implements OnInit {
	@Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
	@Input() rating: string;
	constructor() { }

	ngOnInit(): void {
	}

}
