import { DOCUMENT } from '@angular/common';
import { Optional } from '@angular/core';
import { Component, Inject, Input, OnChanges, OnInit } from '@angular/core';
import { Navigate } from '@ngxs/router-plugin';
import { Select, Store } from '@ngxs/store';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable } from 'rxjs';
import { VIEWER } from 'src/app/core/config/app.config';
import { RelatedPost } from 'src/app/core/interfaces/related-post.interface';
import { AppState } from 'src/app/core/store/state/app.store';

@Component({
	selector: 'blog-similar-posts',
	templateUrl: './similar-posts.component.html',
	styleUrls: ['./similar-posts.component.scss']
})
export class SimilarPostsComponent implements OnInit {
	@Input() id: string;
	@Input() relatedPosts: RelatedPost[];
	@Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
	public isBot: boolean;
    private prevControl:  HTMLElement
    private nextControl:  HTMLElement

	customOptions: OwlOptions = {
        loop: false,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        dots: false,
        navSpeed: 700,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1,
            },
            400: {
                items: 2,
            },
            740: {
                items: 3,
            },
            940: {
                items: 4,
            },
        },
        nav: false,
    };

	constructor(private store: Store, @Inject(DOCUMENT) private dom: Document, @Optional() @Inject(VIEWER) private viewer) {
		this.isBot = this.viewer === 'bot';
    }

	ngOnInit(): void {
        this.nextControl = this.dom.querySelector('.owl-next');
	}
    public onClickNextSlide() {
        this.nextControl = this.dom.querySelector('.owl-next');
        this.nextControl.click();
    }

    public onClickPrevSlide() {
        this.prevControl = this.dom.querySelector('.owl-prev');
        this.prevControl.click();
    }

    public sliderInit(e) {
        // console.log('e', e);
    }

    public onSliderChange(e) {
        const progressBar = this.dom.getElementById(this.id).querySelector('.slider-progress');
        const {startPosition, slides} = e;
        const numOfClick = startPosition-(4 - this.relatedPosts.length);
        // console.log('onSliderChange', startPosition,  progressBar, numOfClick);

    }

}
