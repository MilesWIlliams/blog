import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteSearchbarComponent } from './site-searchbar.component';

describe('SiteSearchbarComponent', () => {
  let component: SiteSearchbarComponent;
  let fixture: ComponentFixture<SiteSearchbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiteSearchbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteSearchbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
