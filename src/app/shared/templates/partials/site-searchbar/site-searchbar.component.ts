import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Store } from '@ngxs/store';
import { SearchPosts } from 'src/app/store/actions/search.actions';

@Component({
	selector: 'blog-site-searchbar',
	templateUrl: './site-searchbar.component.html',
	styleUrls: ['./site-searchbar.component.scss']
})
export class SiteSearchbarComponent implements OnInit, AfterViewInit {
	public searchControl: FormControl;
	@ViewChild('input') searchInput: ElementRef;

	constructor(private fb: FormBuilder, private store: Store) {
		this.searchControl = this.fb.control('');
	}

	ngOnInit(): void { }

	ngAfterViewInit() {
		if (this.searchInput) {
			this.searchInput.nativeElement.addEventListener('keyup', (e) => {
				if (e.key === 'Enter') this.onSearch()
			})
		}
	}

	private onSearch() {
		const query: string = this.searchControl.value;
		if (query.length > 0) {
			this.store.dispatch(new SearchPosts({ query: query }))
		}
	}

}
