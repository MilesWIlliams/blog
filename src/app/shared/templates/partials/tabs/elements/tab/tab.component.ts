import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'blog-tab',
	templateUrl: './tab.component.html',
	styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit {
	@Input() title: string;
	@Input() isActive: boolean;
	constructor() { }

	ngOnInit(): void {
	}

}
