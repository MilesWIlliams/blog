import { Component, Input, OnInit, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/core/store/state/app.store';
import { TabComponent } from '../tab/tab.component';
@Component({
	selector: 'blog-tabs',
	templateUrl: './tabs.component.html',
	styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit, AfterContentInit {
	@Select(AppState.activeBreakpoint) activeBreakPoint$: Observable<string>;
	@ContentChildren(TabComponent) tabs: QueryList<TabComponent>;
	@Input() rating: string;
	constructor() { }

	ngOnInit(): void {
	}

	// contentChildren are set
	ngAfterContentInit() {
		// get all active tabs
		let activeTabs = this.tabs.filter((tab) => tab.isActive);

		// if there is no active tab set, activate the first
		if (activeTabs.length === 0) {
			this.selectTab(this.tabs.first);
		}
	}

	selectTab(tab: TabComponent) {
		// console.log('tab', tab);
		
		// deactivate all tabs
		this.tabs.toArray().forEach(tab => tab.isActive = false);

		// activate the tab the user has clicked on.
		tab.isActive = true;
	}

}
