import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsComponent } from './elements/tabs/tabs.component';
import { TabComponent } from './elements/tab/tab.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UtilsModule } from 'src/app/utils/utils.module';
import { RatingsModule } from '../review-ratings/ratings.module';



@NgModule({
  declarations: [TabsComponent, TabComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UtilsModule,
    RatingsModule
  ],
  exports: [
    TabsComponent,
    TabComponent
  ]
})
export class TabsModule { }
