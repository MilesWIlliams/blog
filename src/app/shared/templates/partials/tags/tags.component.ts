import { Component, Input, OnInit } from '@angular/core';
import { Tag } from 'src/app/core/interfaces/tag.interface';

@Component({
	selector: 'blog-tags',
	templateUrl: './tags.component.html',
	styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {
	@Input() tags: Tag[]
	constructor() { }

	ngOnInit(): void {
	}

}
