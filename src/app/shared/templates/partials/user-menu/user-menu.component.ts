import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AppRoute } from 'src/app/core/interfaces/app-route.interface';
import { NavigationState } from 'src/app/core/store/state/navigation.store';

@Component({
	selector: 'blog-user-menu',
	templateUrl: './user-menu.component.html',
	styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
	@Select(NavigationState.getMenuItems('USER_DROPDOWN')) menuItems$: Observable<AppRoute[]>;
	public showDP: boolean = false;


	constructor(@Inject(DOCUMENT) private dom: Document) { }

	ngOnInit(): void {

		this.dom.addEventListener('click', (ev) => {
			const dpWrapper = this.dom.getElementById('user-menu-lockup');

			if (ev.target !== dpWrapper && !dpWrapper.contains(ev.target as any)) {
				if (this.showDP) this.showDP = false;
			}
		})
	}
}
