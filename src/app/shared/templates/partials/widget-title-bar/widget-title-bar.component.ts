import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'blog-widget-title-bar',
  templateUrl: './widget-title-bar.component.html',
  styleUrls: ['./widget-title-bar.component.scss']
})
export class WidgetTitleBarComponent implements OnInit {
  @Input() title: string;
  @Output() onTitleClick: EventEmitter<string>;
  constructor() { }

  ngOnInit(): void {
  }

  onClick(title) {
    this.onTitleClick.emit(title);
  }

}
