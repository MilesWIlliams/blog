import { AdBanner } from "src/app/core/interfaces/ad-banner.interface";
import { Utils } from "src/app/utils";

export class FetchAllAdBanner  {
    static readonly type = Utils.Helpers.Type('[AdBanner: Fetch] Fetch All');
    constructor() {}
}

export class FetchAllAdBannerSuccess  {
    static readonly type = Utils.Helpers.Type('[AdBanner: Fetch] Fetch All success');
    constructor(public readonly payload: AdBanner[]) {}
}

export class FetchAllAdBannerFail  {
    static readonly type = Utils.Helpers.Type('[AdBanner: Fetch] Fetch All fail');
    constructor(public readonly payload: any) {}
}