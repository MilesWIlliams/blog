import { Utils } from "src/app/utils";
import { Category } from "src/app/core/interfaces/category.interface";
import { FetchPageConfig } from "src/app/core/interfaces/page.interface";


export class FetchAllCategories  {
    static readonly type = Utils.Helpers.Type('[Blog: Fetch] Fetch All blog categories');
    constructor() {}
}

export class FetchAllCategoriesSuccess  {
    static readonly type = Utils.Helpers.Type('[Blog: Fetch] Fetch All blog categories success');
    constructor(public readonly payload: Category[]) {}
}

export class FetchAllCategoriesFail  {
    static readonly type = Utils.Helpers.Type('[Blog: Fetch] Fetch All blog categories fail');
    constructor(public readonly payload: any) {}
}


export class AssignSelectedCategoryPage  {
    static readonly type = Utils.Helpers.Type('[Blog: Assign] Assign selected category page');
    constructor(public readonly payload: FetchPageConfig) {}
}

export class AssignSelectedCategoryPageSuccess  {
    static readonly type = Utils.Helpers.Type('[Blog: Assign] Assign selected category page success');
    constructor(public readonly payload: number) {}
}

export class AssignSelectedCategoryPageFail  {
    static readonly type = Utils.Helpers.Type('[Blog: Assign] Assign selected category page fail');
    constructor(public readonly payload: any) {}
}
