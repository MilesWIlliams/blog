import { BlogPage, FetchPageConfig } from "src/app/core/interfaces/page.interface";
import { Utils } from "src/app/utils";
import { GenericPage } from "src/app/utils/models/generic-page.model";
import { Page } from "src/app/utils/models/page.model";


export class FetchAllPages  {
    static readonly type = Utils.Helpers.Type('[Blog: Fetch] FetchAll blog pages');
    constructor() {}
}

export class FetchAllPagesSuccess  {
    static readonly type = Utils.Helpers.Type('[Blog: Fetch] Fetch All blog pages success');
    constructor(public readonly payload: BlogPage[]) {}
}

export class FetchAllPagesFail  {
    static readonly type = Utils.Helpers.Type('[Blog: Fetch] Fetch All blog pages fail');
    constructor(public readonly payload: any) {}
}


export class AssignPage  {
    static readonly type = Utils.Helpers.Type('[Blog: Assign] Assign page');
    constructor(public readonly payload: FetchPageConfig) {}
}

export class AssignPageSuccess  {
    static readonly type = Utils.Helpers.Type('[Blog: Assign] Assign page success');
    constructor(public readonly payload: Page | GenericPage) {}
}

export class AssignPageFail  {
    static readonly type = Utils.Helpers.Type('[Blog: Assign] Assign page fail');
    constructor(public readonly payload: any) {}
}