import { Utils } from "src/app/utils";
import { Post } from "src/app/core/interfaces/post.interface";
import { PostComment, PostCommentPayload } from "src/app/core/interfaces/comment.interface";

// Fetch posts by category id
export class FetchPostsByCatgoryId  {
    static readonly type = Utils.Helpers.Type('[Posts: CategoryId] Fetch posts by category id');
    constructor(public readonly payload: {id: string}) {}
}

export class FetchPostsByCatgoryIdSuccess  {
    static readonly type = Utils.Helpers.Type('[Posts: CategoryId] Fetch posts by category id success');
    constructor(public readonly payload: Post[]) {}
}

export class FetchPostsByCatgoryIdFail  {
    static readonly type = Utils.Helpers.Type('[Posts: CategoryId] Fetch posts by category id fail');
    constructor(public readonly payload: any) {}
}

export class FetchPostsByTag  {
    static readonly type = Utils.Helpers.Type('[Posts: Tag] Fetch posts by tag');
    constructor(public readonly payload: {tag: string}) {}
}

export class FetchPostsByTagSuccess  {
    static readonly type = Utils.Helpers.Type('[Posts: Tag] Fetch posts by tag success');
    constructor(public readonly payload: Post[]) {}
}

export class FetchPostsByTagFail  {
    static readonly type = Utils.Helpers.Type('[Posts: Tag] Fetch posts by tag fail');
    constructor(public readonly payload: any) {}
}

export class FetchAllPosts  {
    static readonly type = Utils.Helpers.Type('[Posts: Fetcch] Fetch all posts');
    constructor() {}
}

export class FetchAllPostsSuccess  {
    static readonly type = Utils.Helpers.Type('[Posts: Fetcch] Fetch all posts success');
    constructor(public readonly payload: Post[]) {}
}

export class FetchAllPostsFail  {
    static readonly type = Utils.Helpers.Type('[Posts: Fetcch] Fetch all posts fail');
    constructor(public readonly payload: any) {}
}

export class ViewSinglePost {
    static readonly type = Utils.Helpers.Type('[Posts: View] View post');
    constructor(public readonly payload: string) {}
}

export class ViewSinglePostSuccess {
    static readonly type = Utils.Helpers.Type('[Posts: View] View post success');
    constructor(public readonly payload: Post) {}
}

export class ViewSinglePostFail {
    static readonly type = Utils.Helpers.Type('[Posts: View] View post fail');
    constructor(public readonly payload: any) {}
}

export class SubmitPostComment {
    static readonly type = Utils.Helpers.Type('[Posts: Submit] Submit post comment');
    constructor(public readonly payload: PostCommentPayload) {}
}

export class SubmitPostCommentSuccess {
    static readonly type = Utils.Helpers.Type('[Posts: Submit] Submit post comment success');
    constructor(public readonly payload: PostComment) {}
}

export class SubmitPostCommentFail {
    static readonly type = Utils.Helpers.Type('[Posts: Submit] Submit post comment fail');
    constructor(public readonly payload: any) {}
}

export class FetchFeaturedPosts  {
    static readonly type = Utils.Helpers.Type('[Posts: Featured] Fetch featured posts');
    constructor() {}
}

export class FetchFeaturedPostsSuccess  {
    static readonly type = Utils.Helpers.Type('[Posts: Featured] Fetch featured posts success');
    constructor(public readonly payload: Post[]) {}
}

export class FetchFeaturedPostsFail  {
    static readonly type = Utils.Helpers.Type('[Posts: Featured] Fetch featured posts fail');
    constructor(public readonly payload: any) {}
}