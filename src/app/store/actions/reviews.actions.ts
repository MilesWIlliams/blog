import { Utils } from "src/app/utils";
import { Review } from "src/app/core/interfaces/review.interface";
import { PageInfoQuery } from "src/app/types/post.type";

export class FetchReviews  {
    static readonly type = Utils.Helpers.Type('[Reviews: Fetcch] Fetch reviews');
    constructor() {}
}

export class FetchReviewsSuccess  {
    static readonly type = Utils.Helpers.Type('[Reviews: Fetcch] Fetch reviews success');
    constructor(public readonly payload: {pageInfo: PageInfoQuery, reviews: Review[]}) {}
}

export class FetchReviewsFail  {
    static readonly type = Utils.Helpers.Type('[Reviews: Fetcch] Fetch reviews fail');
    constructor(public readonly payload: any) {}
}

export class ViewSingleReview {
    static readonly type = Utils.Helpers.Type('[Reviews: View] View review');
    constructor(public readonly payload: string) {}
}

export class ViewSingleReviewSuccess {
    static readonly type = Utils.Helpers.Type('[Reviews: View] View review success');
    constructor(public readonly payload: Review) {}
}

export class ViewSingleReviewFail {
    static readonly type = Utils.Helpers.Type('[Reviews: View] View review fail');
    constructor(public readonly payload: any) {}
}
