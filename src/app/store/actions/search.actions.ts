import { Post } from "src/app/core/interfaces/post.interface";
import { Utils } from "src/app/utils";

export class SearchPosts {
    static readonly type = Utils.Helpers.Type('[Search] Search posts');
    constructor(public readonly payload: { query: string }) { }
}

export class SearchPostsSuccess {
    static readonly type = Utils.Helpers.Type('[Search] Search posts success');
    constructor(public readonly payload: Post[]) { }
}

export class SearchPostsFail {
    static readonly type = Utils.Helpers.Type('[Search] Search posts fail');
    constructor(public readonly payload: any) { }
}