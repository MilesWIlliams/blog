import { Utils } from "src/app/utils";
import { Tag } from "src/app/core/interfaces/tag.interface";
import { FetchPageConfig } from "src/app/core/interfaces/page.interface";


export class FetchAllTags  {
    static readonly type = Utils.Helpers.Type('[Blog: Fetch] Fetch all blog tags');
    constructor() {}
}

export class FetchAllTagsSuccess  {
    static readonly type = Utils.Helpers.Type('[Blog: Fetch] Fetch all blog tags success');
    constructor(public readonly payload: Tag[]) {}
}

export class FetchAllTagsFail  {
    static readonly type = Utils.Helpers.Type('[Blog: Fetch] Fetch all blog tags fail');
    constructor(public readonly payload: any) {}
}


export class AssignSelectedTag  {
    static readonly type = Utils.Helpers.Type('[Blog: Assign] Assign selected tag page');
    constructor(public readonly payload: FetchPageConfig) {}
}

export class AssignSelectedTagSuccess  {
    static readonly type = Utils.Helpers.Type('[Blog: Assign] Assign selected tag page success');
    constructor(public readonly payload: number) {}
}

export class AssignSelectedTagFail  {
    static readonly type = Utils.Helpers.Type('[Blog: Assign] Assign selected tag page fail');
    constructor(public readonly payload: any) {}
}