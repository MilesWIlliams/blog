import { Utils } from "src/app/utils";
import { Tag } from "src/app/core/interfaces/tag.interface";
import { User } from "src/app/core/interfaces/author.interface";
import { LoginUserPayload, RegisterUserPayload } from "src/app/core/interfaces/app-user.interface";
import { LoginUserResponseQuery, RegisterUserResponseQuery } from "src/app/types/users.type";


export class FetchAllUsers  {
    static readonly type = Utils.Helpers.Type('[Users: Fetch] Fetch all users');
    constructor() {}
}

export class FetchAllUsersSuccess  {
    static readonly type = Utils.Helpers.Type('[Users: Fetch] Fetch all users success');
    constructor(public readonly payload: User[]) {}
}

export class FetchAllUsersFail  {
    static readonly type = Utils.Helpers.Type('[Users: Fetch] Fetch all users fail');
    constructor(public readonly payload: any) {}
}


export class FetchUserByID  {
    static readonly type = Utils.Helpers.Type('[Users: Fetch] Fetch user by id');
    constructor(public readonly payload: number) {}
}

export class FetchUserByIDSuccess  {
    static readonly type = Utils.Helpers.Type('[Users: Fetch] Fetch user by id success');
    constructor(public readonly payload: User) {}
}

export class FetchUserByIDFail  {
    static readonly type = Utils.Helpers.Type('[Users: Fetch] Fetch user by id fail');
    constructor(public readonly payload: any) {}
}
