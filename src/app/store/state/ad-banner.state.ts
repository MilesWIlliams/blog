import { State, Selector, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { Utils } from 'src/app/utils';
import { tap } from 'rxjs/operators';
import { AdBannerService } from 'src/app/core/services/ad-banner.service';
import { FetchAllAdBanner, FetchAllAdBannerFail, FetchAllAdBannerSuccess } from '../actions/ad-banner.actions';
import { AdBanner } from 'src/app/core/interfaces/ad-banner.interface';
import { AdBannerQuery } from 'src/app/types/ad-banner';
;

export interface AdBannersStateModel {
    loaded: boolean;
    loading: boolean;
    banners: {[id: string]:  AdBanner};
}

@State<AdBannersStateModel>({
    name: 'banners',
    defaults: {
        loaded: false,
        loading: false,
        banners: null,
    }
})
@Injectable()
export class AdBannersState {

    constructor(
        private _bannerSvc: AdBannerService,
    ) { }

    @Selector() static isLoading(state: AdBannersStateModel) { return state.loading };
    @Selector() static getAllAdBanners(state: AdBannersStateModel) { return Utils.Helpers.FromHashMap(state.banners) };

    @Action(FetchAllAdBanner)
    fetchAllCategories(ctx: StateContext<AdBannersStateModel>, action: FetchAllAdBanner) {
        ctx.patchState({
            loaded: false,
            loading: true
        });
        return this._bannerSvc.fetchBanners().pipe(
            tap(
                (res) => ctx.dispatch(new FetchAllAdBannerSuccess(this.mapToAdBanners(res))),
                err => ctx.dispatch(new FetchAllAdBannerFail(err))
            )
        );
    }

    @Action(FetchAllAdBannerSuccess)
    fetchAllCategoriesSuccess(ctx: StateContext<AdBannersStateModel>, action: FetchAllAdBannerSuccess) {
        const state = ctx.getState();
        const categories = action.payload;
        ctx.patchState({
            loaded: true,
            loading: false,
            banners: Utils.Helpers.ToHashMap<AdBanner>(categories, state, 'id')
        });
    }

    private mapToAdBanners(data: AdBannerQuery): AdBanner[] {
        if (data.banners.edges.length < 1) return [];

        return data.banners.edges.map((b) => {
            const banner = {
                id: b.node.id,
                bannerInfo: b.node.bannerInfo,
                databaseId: b.node.databaseId,
                date: b.node.date,
                excerpt: b.node.excerpt,
                featuredImage: b.node.featuredImage.node,
                sourceUrl: b.node.featuredImage.node.sourceUrl,
                title: b.node.title,
                link: b.node.featuredImage.node.link
            }

            return banner
        })
    }

}