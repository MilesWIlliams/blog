import { State, Selector, Store, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import * as categoryActions from '../actions/categories.actions';
import { Utils } from 'src/app/utils';
import { tap } from 'rxjs/operators';
import { categoriesQuery } from 'src/app/types/types';
import { Category } from 'src/app/core/interfaces/category.interface';
import { CategoriesService } from 'src/app/core/services/categories.service';
;

export interface CategoriesStateModel {
    categories: { [id: number]: Category },
    loaded: boolean;
    loading: boolean;
    selected_category: string;
}

@State<CategoriesStateModel>({
    name: 'categories',
    defaults: {
        categories: [],
        loaded: false,
        loading: false,
        selected_category: null,
    }
})
@Injectable()
export class CategoriesState {

    constructor(
        private store: Store,
        private _categoerySvc: CategoriesService,
    ) { }

    @Selector() static isLoading(state: CategoriesStateModel) { return state.loading };
    @Selector() static isLoaded(state: CategoriesStateModel) { return state.loaded };
    @Selector() static getAllCategories(state: CategoriesStateModel) { return state.categories };
    @Selector() static getSelectedCategory(state: CategoriesStateModel) { return state.selected_category };

    @Action(categoryActions.AssignSelectedCategoryPage)
    assignSelectedCategoryPage(ctx: StateContext<CategoriesStateModel>, action: categoryActions.AssignSelectedCategoryPage) {
        ctx.patchState({
            selected_category: action.payload.slug
        });
    }

    @Action(categoryActions.FetchAllCategories)
    fetchAllCategories(ctx: StateContext<CategoriesStateModel>, action: categoryActions.FetchAllCategories) {
        ctx.patchState({
            loaded: false,
            loading: true
        });
        return this._categoerySvc.fetchAllCategories().pipe(
            tap(
                (res) => ctx.dispatch(new categoryActions.FetchAllCategoriesSuccess(this.mapToCategoriesCategory(res))),
                err => ctx.dispatch(new categoryActions.FetchAllCategoriesFail(err))
            )
        );
    }

    @Action(categoryActions.FetchAllCategoriesSuccess)
    fetchAllCategoriesSuccess(ctx: StateContext<CategoriesStateModel>, action: categoryActions.FetchAllCategoriesSuccess) {
        const state = ctx.getState();
        const categories = action.payload;
        ctx.patchState({
            loaded: true,
            loading: false,
            categories: Utils.Helpers.ToHashMap<Category>(categories, state, 'databaseId')
        });
    }

    private mapToCategoriesCategory(data: categoriesQuery): Category[] {
        if (data.categories.edges.length < 1) return [];
        return data.categories.edges.map((c) => {
            return {
                id: c.node.id,
                databaseId: c.node.databaseId,
                count: c.node.count,
                description: c.node.description,
                isRestricted: c.node.isRestricted,
                name: c.node.name,
                slug: c.node.slug
            }
        })
    }
}