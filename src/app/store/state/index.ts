
import { AdBannersState } from "./ad-banner.state";
import { CategoriesState } from "./categories.state";
import { PostState } from "./post.store";
import { ReviewState } from "./reviews.state";
import { SearchState } from "./search.state";
import { TagsState } from "./tag.store";
import { TemplateEngineState } from "./template-engine.store";
import { UsersState } from "./users.state";

export const states = [
    TemplateEngineState,
    PostState,
    CategoriesState,
    TagsState,
    UsersState,
    AdBannersState,
    SearchState,
    ReviewState
];