import { State, Selector, Store, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import * as postActions from '../actions/posts.actions';
import { Utils } from 'src/app/utils';

import { tap } from 'rxjs/operators';
import { PostsService } from 'src/app/core/services/posts.service';
import { Post } from 'src/app/core/interfaces/post.interface';
import { GenericPage } from 'src/app/utils/models/generic-page.model';
import { Navigate } from '@ngxs/router-plugin';
import { AssignPageSuccess } from '../actions/page.actions';

export interface PostStateModel {

    loaded: boolean;
    loading: boolean;
    posts: { [id: number]: Post },
    featured_posts: { [id: number]: Post },
    selected_post_id: string;
}

@State<PostStateModel>({
    name: 'posts',
    defaults: {
        loaded: false,
        loading: false,
        posts: [],
        selected_post_id: null,
        featured_posts: null,
    }
})
@Injectable()
export class PostState {

    constructor(
        private store: Store,
        private _postsSvc: PostsService
    ) { }

    @Selector() static isLoading(state: PostStateModel) { return state.loading };
    @Selector() static getAllPosts(state: PostStateModel) { return Utils.Helpers.FromHashMap<Post>(state.posts) };
    @Selector() static getSelectedPost(state: PostStateModel) { return state.posts[state.selected_post_id]; }
    @Selector() static getFeaturedPosts(state: PostStateModel) { return Utils.Helpers.FromHashMap(state.featured_posts); }

    @Action(postActions.FetchAllPosts)
    fetchAllPosts(ctx: StateContext<PostStateModel>, action: postActions.FetchAllPosts) {
        ctx.patchState({
            loaded: false,
            loading: true,
        });
        return this._postsSvc.fetchAllPosts().pipe(
            tap(
                (res) => {
                    const posts = new Utils.Mappers.MapPosts(res);
                    ctx.dispatch(new postActions.FetchAllPostsSuccess(posts.Posts))
                },
                err => ctx.dispatch(new postActions.FetchAllPostsFail(err))
            )
        );
    }

    @Action(postActions.FetchAllPostsSuccess)
    fetchAllPostsSuccess(ctx: StateContext<PostStateModel>, action: postActions.FetchAllPostsSuccess) {
        const state = ctx.getState();
        const posts = action.payload.map(p => new Utils.Mappers.MapToPost(p).Post);

        ctx.patchState({
            loaded: true,
            loading: false,
            posts: Utils.Helpers.ToHashMap<Post>(posts, state, 'slug')
        });
    }
    @Action(postActions.FetchPostsByCatgoryId)
    fetchPostsByCatgoryId(ctx: StateContext<PostStateModel>, action: postActions.FetchPostsByCatgoryId) {
        ctx.patchState({
            loaded: false,
            loading: true,
        });
        return this._postsSvc.fetchPostsbyCategoryId(action.payload.id).pipe(
            tap(
                (res) => {
                    const posts = new Utils.Mappers.CategoryPostToPosts(res);
                    ctx.dispatch(new postActions.FetchPostsByCatgoryIdSuccess(posts.Posts))
                },
                err => ctx.dispatch(new postActions.FetchPostsByCatgoryIdFail(err))
            )
        );
    }

    @Action(postActions.FetchPostsByCatgoryIdSuccess)
    fetchPostsByCatgoryIdSuccess(ctx: StateContext<PostStateModel>, action: postActions.FetchPostsByCatgoryIdSuccess) {
        const state = ctx.getState();
        const posts = action.payload.map(p => new Utils.Mappers.MapToPost(p).Post);

        ctx.patchState({
            loaded: true,
            loading: false,
            posts: Utils.Helpers.ToHashMap<Post>(posts, state, 'slug')
        });
    }
    @Action(postActions.FetchPostsByTag)
    fetchPostsByTag(ctx: StateContext<PostStateModel>, action: postActions.FetchPostsByTag) {
        ctx.patchState({
            loaded: false,
            loading: true,
        });
        return this._postsSvc.getPostsByTag(action.payload.tag).pipe(
            tap(
                (res) => {
                    const posts = new Utils.Mappers.MapPosts(res);
                    ctx.dispatch(new postActions.FetchPostsByTagSuccess(posts.Posts))
                },
                err => ctx.dispatch(new postActions.FetchPostsByTagFail(err))
            )
        );
    }

    @Action(postActions.FetchPostsByTagSuccess)
    fetchPostsByTagSuccess(ctx: StateContext<PostStateModel>, action: postActions.FetchPostsByTagSuccess) {
        const state = ctx.getState();
        const posts = action.payload.map(p => new Utils.Mappers.MapToPost(p).Post);

        ctx.patchState({
            loaded: true,
            loading: false,
            posts: Utils.Helpers.ToHashMap<Post>(posts, state, 'slug')
        });
    }

    @Action(postActions.FetchFeaturedPosts)
    fetchFeaturedPosts(ctx: StateContext<PostStateModel>, action: postActions.FetchFeaturedPosts) {
        ctx.patchState({
            loaded: false,
            loading: true,
        });
        return this._postsSvc.getFeaturedPosts().pipe(
            tap(
                (res) => {
                    console.log('res: ', res);
                    
                    const posts = new Utils.Mappers.MapPosts(res);
                    ctx.dispatch(new postActions.FetchFeaturedPostsSuccess(posts.Posts))
                },
                err => ctx.dispatch(new postActions.FetchFeaturedPostsFail(err))
            )
        );
    }

    @Action(postActions.FetchFeaturedPostsSuccess)
    fetchFeaturedPostsSuccess(ctx: StateContext<PostStateModel>, action: postActions.FetchFeaturedPostsSuccess) {
        const state = ctx.getState();
        const posts = action.payload.map(p => new Utils.Mappers.MapToPost(p).Post);

        ctx.patchState({
            loaded: true,
            loading: false,
            featured_posts: Utils.Helpers.ToHashMap<Post>(posts, state, 'slug')
        });
    }

    @Action(postActions.ViewSinglePost)
    viewSinglePost(ctx: StateContext<PostStateModel>, action: postActions.ViewSinglePost) {
        const state = ctx.getState();
        const posts = Utils.Helpers.FromHashMap(state.posts);
        const slug = action.payload;

        ctx.patchState({
            loading: true,
            loaded: false,
        });

        if (posts.length > 0) {
            if (state.posts[slug] !== undefined)  {
                const post = state.posts[slug] as Post;
                const page = new GenericPage({slug: post.slug, type: 'post', title: post.title});
                ctx.patchState({
                    loading: false,
                    loaded: true,
                    selected_post_id: slug
                });
                return this.store.dispatch([
                    new AssignPageSuccess(page),
                    new  Navigate([`/posts/${slug}`])]);
            }

            return this._postsSvc.fetchPostsbyId(slug).pipe(
                tap(
                    (res) => ctx.dispatch(new postActions.ViewSinglePostSuccess(new Utils.Mappers.MapToPost(res).Post as any)),
                    err => ctx.dispatch(new postActions.ViewSinglePostFail(err))
                )
            );
        }

        return this._postsSvc.fetchPostsbyId(slug).pipe(
            tap(
                (res) => {
                    const p = new Utils.Mappers.MapToPost(res).Post;

                    ctx.dispatch(new postActions.ViewSinglePostSuccess(new Utils.Mappers.MapToPost(res).Post as any))
                },
                err => ctx.dispatch(new postActions.ViewSinglePostFail(err))
            )
        );

    }

    @Action(postActions.ViewSinglePostSuccess)
    viewSinglePostSuccess(ctx: StateContext<PostStateModel>, action: postActions.ViewSinglePostSuccess) {
        const state = ctx.getState();

        const posts = Utils.Helpers.FromHashMap(state.posts);
        const post = action.payload;
        const page = new GenericPage({slug: post.slug, type: 'post', title: post.title});

        posts.push(post);
        ctx.patchState({
            loaded: false,
            loading: true,
            posts: Utils.Helpers.ToHashMap<Post>(posts, state, 'slug'),
            selected_post_id: post.slug
        });

        return this.store.dispatch([
            new AssignPageSuccess(page),
            new  Navigate([`/posts/${post.slug}`])
        ]);
    }

    @Action(postActions.SubmitPostComment)
    submitPostComment(ctx: StateContext<PostStateModel>, action: postActions.SubmitPostComment) {
        const state = ctx.getState();
        const commentDetails = action.payload;
        ctx.patchState({
            loading: true,
            loaded: false,
        });

        if (commentDetails) {
            return this._postsSvc.postComment(commentDetails).pipe(
                tap(
                    (res) => {
                        if (res.createComment.success) {
                            ctx.dispatch(new postActions.SubmitPostCommentSuccess(res.createComment.comment))
                        } else {
                            ctx.dispatch(new postActions.SubmitPostCommentFail(res))
                        }
                    },
                    err => ctx.dispatch(new postActions.SubmitPostCommentFail(err))
                )
            );
        }
    }

    @Action(postActions.SubmitPostCommentSuccess)
    submitPostCommentSuccess(ctx: StateContext<PostStateModel>, action: postActions.SubmitPostCommentSuccess) {
        const state = ctx.getState();
        const selectedPostID = state.selected_post_id

        let selectedPost: Post = Object.assign({}, state.posts[selectedPostID]);
        let allPosts = Utils.Helpers.FromHashMap<Post>(state.posts).filter(p => p.slug !== selectedPostID);
        let comments = [];

        if (Array.isArray(selectedPost.comments)) {
            comments = [...selectedPost.comments];
        }
        const newComment = action.payload;
        newComment.author = action.payload.author['node'];
        comments.push(newComment);
        selectedPost.comments = comments;
        selectedPost.commentCount = comments.length;

        allPosts.push(selectedPost);
        ctx.patchState({
            loaded: false,
            loading: true,
            posts: Utils.Helpers.ToHashMap(allPosts, state, 'slug')
        });

    }
}