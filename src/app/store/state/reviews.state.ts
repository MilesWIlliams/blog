import { State, Selector, Action, StateContext, Store } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { Utils } from 'src/app/utils';
import { tap } from 'rxjs/operators';
import { FetchAllAdBannerFail } from '../actions/ad-banner.actions';
import { Review } from 'src/app/core/interfaces/review.interface';
import { FetchReviews, FetchReviewsSuccess, ViewSingleReview, ViewSingleReviewFail, ViewSingleReviewSuccess } from '../actions/reviews.actions';
import { ReviewsService } from 'src/app/core/services/reviews.service';
import { PageInfoQuery } from 'src/app/types/post.type';
import { GenericPage } from 'src/app/utils/models/generic-page.model';
import { AssignPageSuccess } from '../actions/page.actions';
import { Navigate } from '@ngxs/router-plugin';
;

export interface ReviewStateModel {
    loaded: boolean;
    loading: boolean;
    page_info: PageInfoQuery
    reviews: { [id: string]: Review };
    selected_review: string
}

@State<ReviewStateModel>({
    name: 'reviews',
    defaults: {
        loaded: false,
        loading: false,
        reviews: {},
        page_info: null,
        selected_review: null
    }
})
@Injectable()
export class ReviewState {

    constructor(
        private _svc: ReviewsService,
        private  store: Store
    ) { }

    @Selector() static isLoading(state: ReviewStateModel) { return state.loading };
    @Selector() static isLoaded(state: ReviewStateModel) { return state.loaded };
    @Selector() static getReviews(state: ReviewStateModel) { return Utils.Helpers.FromHashMap<Review>(state.reviews) };
    @Selector() static getSelectedReview(state: ReviewStateModel) { return state.reviews[state.selected_review] };
    @Selector() static getPageInfo(state: ReviewStateModel) { return state.page_info };

    @Action(FetchReviews)
    fetchReviews(ctx: StateContext<ReviewStateModel>, action: FetchReviews) {
        ctx.patchState({
            loaded: false,
            loading: true
        });
        return this._svc.fetchReviews().pipe(
            tap(
                (res) => ctx.dispatch(new FetchReviewsSuccess({ pageInfo: res.reviews.pageInfo, reviews: res.reviews.nodes })),
                err => ctx.dispatch(new FetchAllAdBannerFail(err))
            )
        );
    }

    @Action(FetchReviewsSuccess)
    fetchAllCategoriesSuccess(ctx: StateContext<ReviewStateModel>, action: FetchReviewsSuccess) {
        const state = ctx.getState();
        const reviews = action.payload;
        ctx.patchState({
            loaded: true,
            loading: false,
            page_info: reviews.pageInfo,
            reviews: Utils.Helpers.ToHashMap<Review>(reviews.reviews, state, 'slug')
        });
    }

    @Action(ViewSingleReview)
    viewSinglePost(ctx: StateContext<ReviewStateModel>, action: ViewSingleReview) {
        const state = ctx.getState();
        const posts = Utils.Helpers.FromHashMap(state.reviews);
        const slug = action.payload;

        ctx.patchState({
            loading: true,
            loaded: false,
        });

        if (posts.length > 0) {
            if (state.reviews[slug] !== undefined)  {
                const post = state.reviews[slug] as Review;
                const page = new GenericPage({slug: post.slug, type: 'reviews', title: post.title});
                ctx.patchState({
                    loading: false,
                    loaded: true,
                    selected_review: slug
                });
                return this.store.dispatch([
                    new AssignPageSuccess(page),
                    new  Navigate([`/reviews/${slug}`])]);
            }

            return this._svc.fetchReviewBySlug(slug).pipe(
                tap(
                    (res) => ctx.dispatch(new ViewSingleReviewSuccess(res.reviewBy)),
                    err => ctx.dispatch(new ViewSingleReviewFail(err))
                )
            );
        }

        return this._svc.fetchReviewBySlug(slug).pipe(
            tap(
                (res) => {
                    ctx.dispatch(new ViewSingleReviewSuccess(res.reviewBy))
                },
                err => ctx.dispatch(new ViewSingleReviewFail(err))
            )
        );

    }

    @Action(ViewSingleReviewSuccess)
    viewSinglePostSuccess(ctx: StateContext<ReviewStateModel>, action: ViewSingleReviewSuccess) {
        const state = ctx.getState();

        const posts = Utils.Helpers.FromHashMap(state.reviews);
        const post = action.payload;
        const page = new GenericPage({slug: post.slug, type: 'reviews', title: post.title});

        posts.push(post);
        ctx.patchState({
            loaded: false,
            loading: true,
            reviews: Utils.Helpers.ToHashMap<Review>(posts, state, 'slug'),
            selected_review: post.slug
        });

        return this.store.dispatch([
            new AssignPageSuccess(page),
            new  Navigate([`/reviews/${post.slug}`])
        ]);
    }

}