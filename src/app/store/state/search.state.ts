import { State, Selector, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { Utils } from 'src/app/utils';
import { tap } from 'rxjs/operators';
import { AdBannerService } from 'src/app/core/services/ad-banner.service';
import { FetchAllAdBanner, FetchAllAdBannerFail, FetchAllAdBannerSuccess } from '../actions/ad-banner.actions';
import { AdBanner } from 'src/app/core/interfaces/ad-banner.interface';
import { AdBannerQuery } from 'src/app/types/ad-banner';
import { Post } from 'src/app/core/interfaces/post.interface';
import { PostsService } from 'src/app/core/services/posts.service';
import { SearchPosts, SearchPostsSuccess } from '../actions/search.actions';
import { Navigate } from '@ngxs/router-plugin';
;

export interface SearchStateModel {
    loaded: boolean;
    loading: boolean;
    banners: {[id: string]:  AdBanner};
    query: string;
    search_results: {[slug: string]: Post}
}

@State<SearchStateModel>({
    name: 'search',
    defaults: {
        loaded: false,
        loading: false,
        query: null,
        search_results: {},
        banners: null,
    }
})
@Injectable()
export class SearchState {

    constructor(
        private _postSvc: PostsService
    ) { }

    @Selector() static isLoading(state: SearchStateModel) { return state.loading };
    @Selector() static isLoaded(state: SearchStateModel) { return state.loaded };
    @Selector() static getSearchResults(state: SearchStateModel) { return Utils.Helpers.FromHashMap<Post>(state.search_results) };
    @Selector() static getQueryString(state: SearchStateModel) { return state.query };

    @Action(SearchPosts)
    fetchAllCategories(ctx: StateContext<SearchStateModel>, action: SearchPosts) {
        const {query} = action.payload;
        ctx.patchState({
            loaded: false,
            loading: true,
            query: query
        });
        return this._postSvc.searchPosts(query).pipe(
            tap(
                (res) => ctx.dispatch(new SearchPostsSuccess(new Utils.Mappers.MapPosts(res).Posts)),
                err => ctx.dispatch(new FetchAllAdBannerFail(err))
            )
        );
    }

    @Action(SearchPostsSuccess)
    fetchAllCategoriesSuccess(ctx: StateContext<SearchStateModel>, action: SearchPostsSuccess) {
        const state = ctx.getState();
        const searchResults = action.payload;
        ctx.patchState({
            loaded: true,
            loading: false,
            search_results: Utils.Helpers.ToHashMap<Post>(searchResults, state, 'slug')
        });

        return ctx.dispatch(new Navigate(['/search'], {query: state.query}));
    }



}