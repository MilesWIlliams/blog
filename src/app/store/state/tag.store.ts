import { State, Selector, Store, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import * as tagActions from '../actions/tags.actions';
import { Utils } from 'src/app/utils';
import { tap } from 'rxjs/operators';
import { tagQuery } from 'src/app/types/types';
import { Tag } from 'src/app/core/interfaces/tag.interface';
import { TagsService } from 'src/app/core/services/tag.service';

export interface TagsStateModel {
    tags: { [id: number]: Tag },
    loaded: boolean;
    loading: boolean;
    selected_tag: string;
}

@State<TagsStateModel>({
    name: 'tags',
    defaults: {
        tags: [],
        loaded: false,
        loading: false,
        selected_tag: null
    }
})
@Injectable()
export class TagsState {

    constructor(
        private store: Store,
        private _tagSvc: TagsService,
    ) { }

    @Selector() static isLoading(state: TagsStateModel) { return state.loading };
    @Selector() static isLoaded(state: TagsStateModel) { return state.loaded };
    @Selector() static allTags(state: TagsStateModel) { return state.tags };
    @Selector() static getSelectedTag(state: TagsStateModel) { return state.selected_tag };

    @Action(tagActions.FetchAllTags)
    fetchAllTags(ctx: StateContext<TagsStateModel>, action: tagActions.FetchAllTags) {
        ctx.patchState({
            loaded: false,
            loading: true
        });
        return this._tagSvc.fetchAllTags().pipe(
            tap(
                (res) => ctx.dispatch(new tagActions.FetchAllTagsSuccess(this.mapToBlogTag(res))),
                err => ctx.dispatch(new tagActions.FetchAllTagsFail(err))
            )
        );
    }

    @Action(tagActions.FetchAllTagsSuccess)
    fetchAllTagsSuccess(ctx: StateContext<TagsStateModel>, action: tagActions.FetchAllTagsSuccess) {
        const state = ctx.getState();
        const tags = action.payload;
        ctx.patchState({
            loaded: true,
            loading: false,
            tags: Utils.Helpers.ToHashMap<Tag>(tags, state, 'tagId')
        });
    }

    private mapToBlogTag(data: tagQuery): Tag[] {
        if (data.tags.edges.length < 1) return [];
        return data.tags.edges.map((t) => {
            return {
                id: t.node.id,
                count: t.node.count,
                description: t.node.description,
                isRestricted: t.node.isRestricted,
                name: t.node.name,
                slug: t.node.slug,
                tagId: t.node.tagId
            }
        })
    }

}