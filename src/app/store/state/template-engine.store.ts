import { State, Selector, Store, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import * as pageActions from '../actions/page.actions';
import * as categoryActions from '../actions/categories.actions';
import * as postActions from '../actions/posts.actions';
import { Utils } from 'src/app/utils';
import { BlogPage } from 'src/app/core/interfaces/page.interface';
import { BlogPageService } from 'src/app/core/services/blog-page.service';
import { tap } from 'rxjs/operators';
import { pageQuery } from 'src/app/types/types';
import { Tag } from 'src/app/core/interfaces/tag.interface';
import { Category } from 'src/app/core/interfaces/category.interface';

import { PageTypeEnum } from 'src/app/core/enums/page-type.enum';
import { PostsService } from 'src/app/core/services/posts.service';
import { Page } from 'src/app/utils/models/page.model';
import { GenericPage } from 'src/app/utils/models/generic-page.model';
import { CategoriesState } from './categories.state';
import { TagsState } from './tag.store';
import { AssignSelectedCategoryPage } from '../actions/categories.actions';
import { PostState } from './post.store';
import { Post } from 'src/app/core/interfaces/post.interface';
import { AssignSelectedTag } from '../actions/tags.actions';
import { ReviewState } from './reviews.state';
import { Review } from 'src/app/core/interfaces/review.interface';
import { ViewSingleReview } from '../actions/reviews.actions';

export interface TemplateEngineStateModel {
    loaded: boolean;
    loading: boolean;
    pages: { [databaseId: number]: BlogPage };
    selected_page_object: Page | GenericPage;
    selected_page_id: number;
}

@State<TemplateEngineStateModel>({
    name: 'templateEngine',
    defaults: {
        loaded: false,
        loading: false,
        pages: null,
        selected_page_id: null,
        selected_page_object: null,
    }
})
@Injectable()
export class TemplateEngineState {

    constructor(
        private store: Store,
        private _pageSvc: BlogPageService,
        private _postsSvc: PostsService
    ) { }

    @Selector()
    static pageType(state: TemplateEngineStateModel) { return state.selected_page_object.type };

    @Selector([CategoriesState.isLoaded, TagsState.isLoaded])
    static isLoaded(state: TemplateEngineStateModel, catgoriesLoaded, tagsLoaded) { return (state.loaded && catgoriesLoaded && tagsLoaded) };
    @Selector() static isLoading(state: TemplateEngineStateModel) { return state.loading };
    @Selector() static getSelectedPage(state: TemplateEngineStateModel) { return state.selected_page_object; }

    @Action(pageActions.FetchAllPages)
    fetchAllPages(ctx: StateContext<TemplateEngineStateModel>, action: pageActions.FetchAllPages) {
        ctx.patchState({
            loaded: false,
            loading: true
        });
        return this._pageSvc.fetchAllPages().pipe(
            tap(
                (res) => ctx.dispatch(new pageActions.FetchAllPagesSuccess(this.mapToBlogPages(res))),
                err => ctx.dispatch(new pageActions.FetchAllPagesFail(err))
            )
        );
    }

    @Action(pageActions.FetchAllPagesSuccess)
    fetchAllPagesSuccess(ctx: StateContext<TemplateEngineStateModel>, action: pageActions.FetchAllPagesSuccess) {
        const state = ctx.getState();
        const pages = action.payload;

        ctx.patchState({
            pages: Utils.Helpers.ToHashMap(pages, state, 'databaseId'),
            loaded: true,
            loading: false,
        });
    }

    @Action(pageActions.AssignPage)
    assignPage(ctx: StateContext<TemplateEngineStateModel>, action: pageActions.AssignPage) {
        const config = action.payload;
        const state = ctx.getState();

        switch (config.type) {
            case PageTypeEnum.Page: {
                const pages = Utils.Helpers.FromHashMap<BlogPage>(state.pages);

                let selectedPage = pages.find(p => p.slug === config.slug);
                if (selectedPage) {
                    ctx.patchState({
                        selected_page_id: selectedPage.databaseId
                    });
                }

                if (selectedPage.isFrontPage) {

                    return this._pageSvc.fetchFrontPage(selectedPage.id).pipe(
                        tap(
                            (res) => {
                                ctx.dispatch(new pageActions.AssignPageSuccess(new Page(res)))
                            },
                            err => ctx.dispatch(new categoryActions.FetchAllCategoriesFail(err))
                        )
                    )
                } else {

                    return ctx.dispatch(new pageActions.AssignPageSuccess(new Page({...selectedPage, type: PageTypeEnum.Page})))
                }
            }


            case PageTypeEnum.Category:  {
                const categories = Utils.Helpers.FromHashMap<Category>(this.store.selectSnapshot(CategoriesState.getAllCategories));

                const selectedCategory = categories.find(c => c.slug === config.slug);
                const genericPage = new GenericPage({title: selectedCategory.name, type: PageTypeEnum.Category, slug: config.slug});
                ctx.patchState({
                    selected_page_id: selectedCategory.databaseId
                });

                return ctx.dispatch([
                    new AssignSelectedCategoryPage(config),
                    new postActions.FetchPostsByCatgoryId({ id: selectedCategory.id }),
                    new pageActions.AssignPageSuccess(genericPage)
                ]);
            }

            case PageTypeEnum.Post:  {
                return ctx.dispatch(new postActions.ViewSinglePost(config.slug));
            }


            case PageTypeEnum.Tag: {
                const tags = Utils.Helpers.FromHashMap<Tag>(this.store.selectSnapshot(TagsState.allTags));
                const selectedTag = tags.find(t => t.slug === config.slug);
                const genericPage = new GenericPage({title: selectedTag.name, type: PageTypeEnum.Tag, slug: config.slug});
                ctx.patchState({
                    selected_page_id: selectedTag.tagId
                });

                return ctx.dispatch([
                    new AssignSelectedTag(config),
                    new postActions.FetchPostsByTag({ tag: selectedTag.name }),
                    new pageActions.AssignPageSuccess(genericPage)
                ]);
            }
            case PageTypeEnum.Review: {
                return ctx.dispatch(new ViewSingleReview(config.slug));
            }

            default:
                break;
        }

    }

    @Action(pageActions.AssignPageSuccess)
    assignPageSuccess(ctx: StateContext<TemplateEngineStateModel>, action: pageActions.AssignPageSuccess) {
        const page = action.payload;

        ctx.patchState({
            selected_page_object: page
        });
    }

    private mapToBlogPages(data: pageQuery): BlogPage[] {
        return data.pages.edges.map((p) => {
            return {
                id: p.node.id,
                content: p.node.content,
                isFrontPage: p.node.isFrontPage,
                featuredImage: p.node.featuredImage?.node,
                databaseId: p.node.databaseId,
                slug: p.node.slug,
                title: p.node.title
            }
        })
    }

}