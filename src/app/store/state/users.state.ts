import { State, Selector, Store, Action, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import * as usersActions from '../actions/users.actions';
import { Utils } from 'src/app/utils';
import { tap } from 'rxjs/operators';
import { TagsService } from 'src/app/core/services/tag.service';
import { User } from 'src/app/core/interfaces/author.interface';
import { UsersService } from 'src/app/core/services/user.service';
import { userByIDQuery, usersQuery } from 'src/app/types/users.type';
import { Navigate } from '@ngxs/router-plugin';

export interface UsersStateModel {
    users: {[id: string]: User}
    loaded: boolean;
    loading: boolean;
}

@State<UsersStateModel>({
    name: 'users',
    defaults: {
        users: {},
        loaded: false,
        loading: false,
    }
})
@Injectable()
export class UsersState {

    constructor(
        private _usersSvc: UsersService
    ) { }

    @Selector() static isLoading(state: UsersStateModel) { return state.loading };
    @Selector() static getAllUsers(state: UsersStateModel) { return Utils.Helpers.FromHashMap(state.users)};
    @Selector() static getAllUsersMapped(state: UsersStateModel) { return state.users};
    @Selector()
    static getUserByID(state: UsersStateModel) {
        return (id: string) => {
            return state.users[id]
        }
    };


    @Action(usersActions.FetchAllUsers)
    fetchAllUsers(ctx: StateContext<UsersStateModel>, action: usersActions.FetchAllUsers) {
        ctx.patchState({
            loaded: false,
            loading: true
        });
        return this._usersSvc.fetchAllUsers().pipe(
            tap(
                (res) => ctx.dispatch(new usersActions.FetchAllUsersSuccess(this.mapToUsers(res))),
                (err: any) => ctx.dispatch(new usersActions.FetchAllUsersFail(err))
            )
        );
    }

    @Action(usersActions.FetchAllUsersSuccess)
    fetchAllUsersSuccess(ctx: StateContext<UsersStateModel>, action: usersActions.FetchAllUsersSuccess) {
        const state = ctx.getState();
        const tags = action.payload;
        ctx.patchState({
            loaded: true,
            loading: false,
            users: Utils.Helpers.ToHashMap<User>(tags, state, 'tagId')
        });
    }

    @Action(usersActions.FetchUserByID)
    fetchUserByID(ctx: StateContext<UsersStateModel>, action: usersActions.FetchUserByID) {
        const state = ctx.getState();

        const hasUserLoaded = Boolean(state.users[action.payload]);
        if (hasUserLoaded) return;

        return this._usersSvc.fetchUserByID(action.payload).pipe(
            tap(
                (res) => ctx.dispatch(new usersActions.FetchUserByIDSuccess(this.mapToUser(res))),
                (err: any) => ctx.dispatch(new usersActions.FetchUserByIDFail(err))
            )
        );
    }

    @Action(usersActions.FetchUserByIDSuccess)
    fetchUserByIDSuccess(ctx: StateContext<UsersStateModel>, action: usersActions.FetchUserByIDSuccess) {
        const state = ctx.getState();
        const allUsers = Utils.Helpers.FromHashMap(state.users);
        const user = action.payload;

        allUsers.push(user);
        ctx.patchState({
            loaded: true,
            loading: false,
            users: Utils.Helpers.ToHashMap<User>(allUsers, state, 'databaseId')
        });
    }

    private mapToUsers(data: usersQuery): User[] {
        return data.users.edges.map(n =>n.node);
    }

    private mapToUser(data: userByIDQuery): User {

        return data.user
    }

}