import { BannerInfo } from "../core/interfaces/ad-banner.interface";

export interface AdBannerQuery {
    banners: {
        edges: Array<{
            node: {
                id: string,
                bannerInfo: BannerInfo,
                databaseId: number,
                excerpt:  string,
                link: string,
                date: string,
                featuredImage: {
                    node: {
                        srcSet: string,
                        sourceUrl: string,
                        link?: string
                    }
                }
                title: string
            }
        }>,
    }

}