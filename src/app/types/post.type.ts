import { Tag } from "../core/interfaces/tag.interface";

export interface relatedPostsQuery {
    edges:Array<{
        node: {
            id: string,
            title: string,
            slug: string,
            featuredImage: {
                node: {
                    uri: string,
                    srcSet: string,
                    link: string,
                    title: string,
                    slug: string,
                }
            }
        }
    }>
}

export interface PageInfoQuery {
    hasNextPage: boolean,
    hasPreviousPage: boolean,
    startCursor: string,
    endCursor: string
}
export interface postQueryData {
    id: string,
    authorId: string,
    author: AuthorQuery,
    categories: {
        edges: Array<{
            node: {
                id: string,
                count: number,
                databaseId: number,
                description: string,
                isRestricted: boolean,
                name: string,
                slug: string
            }
        }>
    },
    commentCount: number,
    comments: {
        nodes: Array<{
            id: string,
            databaseId: number,
            date: string,
            approved: boolean,
            content: string,
            author: AuthorQuery
        }>
    },
    content: string,
    cursor: string,
    databaseId: number,
    date: string,
    enqueudScripts: PageInfoQuery,
    excerpt: string,
    title: string,
    featuredImageId: string,
    featuredImage: {
        node: {
            uri: string,
            srcSet: string,
            link: string,
            title: string,
            slug: string,
        }
    }
    next: {
        slug: string
    }
    isRestricted: boolean,
    isPreview: boolean,
    pageInfo: PageInfoQuery,
    previous: {
        slug: string
    }
    relatedPosts: relatedPostsQuery,
    tags: {
        edges: Array<{
            node: Tag
        }>
    },
    slug: string
}

export interface AuthorQuery {
    node: {
        avatar: {
            url: string
        },
        id: string,
        databaseId: number,
        description:  string;
        name: string,
    }
}