import { FeaturedImage } from "../core/interfaces/media.interface";
import { Review } from "../core/interfaces/review.interface";
import { Tag } from "../core/interfaces/tag.interface";
import { AuthorQuery, PageInfoQuery, postQueryData } from "./post.type";

export interface menuQuery {
    edges: Array<{
        node: {
            id: string,
            label: string,
            path: string
        }
    }>
}


export interface MenusQuery {
    menus: {
        edges: Array<{
            node: {
                databaseId: number,
                name: string,
                slug: string,
                locations: string[],
                menuItems: menuQuery
            }
        }>
    }
}

export interface postsQuery {
    posts: {
        pageInfo: PageInfoQuery
        edges: Array<{
            cursor: string,
            node: {
                id: string,
                authorId: string,
                author: AuthorQuery,
                categories: {
                    edges: Array<{
                        node: {
                            id: string,
                            count: number,
                            databaseId: number,
                            description: string,
                            isRestricted: boolean,
                            name: string,
                            slug: string
                        }
                    }>
                },
                commentCount: number,
                comments?: {
                    nodes: Array<{
                        id: string,
                        databaseId: number,
                        date: string,
                        approved: boolean,
                        content: string,
                        author: AuthorQuery
                    }>
                },
                content: string,
                databaseId: number,
                date: string,
                excerpt: string,
                title: string,
                featuredImageId: string,
                featuredImage: {
                    node: {
                        altText?: string,
                        uri: string,
                        srcSet: string,
                        link: string,
                        title: string,
                        slug: string,
                    }
                }
                isRestricted: boolean,
                isPreview: boolean,
                next: {
                    slug: string
                }
                previous: {
                    slug: string
                }
                relatedPosts: {
                    edges: Array<{
                        node: {
                            id: string,
                            title: string,
                            slug: string,
                            next: {
                                slug: string
                            }
                            previous: {
                                slug: string
                            }
                            featuredImage: {
                                node: {
                                    uri: string,
                                    srcSet: string,
                                    link: string,
                                    title: string,
                                    slug: string,
                                }
                            }
                        }
                    }>
                };
                tags: {
                    edges: Array<{
                        node: {
                            id: string,
                            count: number,
                            description: string,
                            isRestricted: boolean,
                            name: string,
                            slug: string,
                            tagId: number
                        }
                    }>
                };
                slug: string
            }
        }>
    }
}


export interface pageQuery {
    pages: {
        edges: Array<{
            node: {
                id: string,
                isFrontPage: boolean,
                slug: string,
                content: string,
                title: string,
                databaseId: number,
                featuredImage: {
                    node: {
                        altText?: string,
                        srcSet: string,
                        link: string,
                        title: string,
                    }
                }
            }
        }>
    }
}

export interface singlePageQuery {
    page: {
        id: string,
        isFrontPage: boolean,
        slug: string,
        content: string,
        title: string,
        databaseId: number,
    }
}

export interface categoriesQuery {
    categories: {
        edges: Array<{
            node: {
                id: string,
                databaseId: number,
                count: number,
                description: string,
                isRestricted: boolean,
                name: string,
                slug: string,
            }
        }>
    }
}

export interface postsByCategoryQuery {
    category: {
        posts: {
            pageInfo: PageInfoQuery
            edges: Array<{
                cursor: string,
                node: {
                    id: string,
                    authorId: string,
                    author: AuthorQuery,
                    categories: {
                        edges: Array<{
                            node: {
                                id: string,
                                count: number,
                                databaseId: number,
                                description: string,
                                isRestricted: boolean,
                                name: string,
                                slug: string
                            }
                        }>
                    },
                    commentCount: number,
                    comments?: {
                        nodes: Array<{
                            id: string,
                            databaseId: number,
                            date: string,
                            approved: boolean,
                            content: string,
                            author: AuthorQuery
                        }>
                    },
                    content: string,
                    databaseId: number,
                    date: string,
                    excerpt: string,
                    title: string,
                    featuredImageId: string,
                    featuredImage: {
                        node: {
                            altText?: string,
                            uri: string,
                            srcSet: string,
                            link: string,
                            title: string,
                            slug: string,
                        }
                    }
                    isRestricted: boolean,
                    isPreview: boolean,
                    next: {
                        slug: string
                    }
                    previous: {
                        slug: string
                    }
                    relatedPosts: {
                        edges: Array<{
                            node: {
                                id: string,
                                title: string,
                                slug: string,
                                next: {
                                    slug: string
                                }
                                previous: {
                                    slug: string
                                }
                                featuredImage: {
                                    node: {
                                        uri: string,
                                        srcSet: string,
                                        link: string,
                                        title: string,
                                        slug: string,
                                    }
                                }
                            }
                        }>
                    };
                    tags: {
                        edges: Array<{
                            node: {
                                id: string,
                                count: number,
                                description: string,
                                isRestricted: boolean,
                                name: string,
                                slug: string,
                                tagId: number
                            }
                        }>
                    };
                    slug: string
                }
            }>
        }
    }
}

export interface postsByIDQuery {
    postBy: postQueryData
}

export interface postsByPageQuery {
    page: {
        posts: {
            edges: Array<{
                cursor: string,
                node: {
                    id: string,
                    authorId: string,

                    categories: {
                        edges: Array<{
                            node: {
                                id: string,
                                count: number,
                                databaseId: number,
                                description: string,
                                isRestricted: boolean,
                                name: string,
                                slug: string
                            }
                        }>
                    },
                    comments: {
                        nodes: Array<{
                            id: string,
                            databaseId: number,
                            date: string,
                            approved: boolean,
                            content: string,
                            author: AuthorQuery
                        }>
                    },
                    content: string,
                    databaseId: number,
                    date: string,
                    title: string,
                    featuredImageId: string,
                    featuredImage: {
                        node: {
                            uri: string,
                            srcSet: string,
                            link: string,
                            title: string,
                            slug: string,
                        }
                    }
                    isRestricted: boolean,
                    isPreview: boolean,
                    next: {
                        slug: string
                    }
                    previous: {
                        slug: string
                    }
                }
            }>
        }
    }
}

export interface frontPageDetailsQuery {
    page: {
        id: string,
        isFrontPage: boolean,
        slug: string,
        content: string,
        title: string,
        databaseId: number,
        featuredImage: {
            node: {
                altText?: string,
                srcSet: string,
                link: string,
                title: string,
            }
        },
        postsGroups: {
            fieldGroupName: string,
            categoryPosts: {
                fieldGroupName: string,
                financesSection: {
                    fieldGroupName: string,
                    posts: Array<frontPagePostCategorySection>,
                    sectionTitle: string,
                },
                investmentSection: {
                    fieldGroupName: string,
                    posts: Array<frontPagePostCategorySection>,
                    sectionTitle: string,
                },
                propertySection: {
                    fieldGroupName: string,
                    posts: Array<frontPagePostCategorySection>,
                    sectionTitle: string,
                },
                taxSection: {
                    fieldGroupName: string,
                    posts: Array<frontPagePostCategorySection>,
                    sectionTitle: string,
                }
            }
        }
    }
}

export interface frontPagePostCategorySection {
    id: string,
    slug: string,
    title: string,
    categories: {
        nodes: Array<{
            id: string,
            categoryId: number,
            count: number,
            databaseId: number,
            description: string,
            isRestricted: boolean,
            name: string,
            slug: string
        }>
    }
    content: string,
    excerpt: string,
    featuredImage: FeaturedImage,
    modified: string,
    commentCount: number
}

export interface tagQuery {
    tags: {
        edges: Array<{
            node: {
                id: string,
                count: number,
                description: string,
                isRestricted: boolean,
                name: string,
                slug: string,
                tagId: number
            }
        }>
    }
}

export interface PostsQuery {
    posts: {
        edges: Array<{
            cursor: string,
            node: {
                id: string,
                authorId: string,
                author: AuthorQuery,
                categories: {
                    edges: Array<{
                        node: {
                            id: string,
                            count: number,
                            databaseId: number,
                            description: string,
                            isRestricted: boolean,
                            name: string,
                            slug: string
                        }
                    }>
                },
                commentCount: number,
                comments?: {
                    nodes: Array<{
                        id: string,
                        databaseId: number,
                        date: string,
                        approved: boolean,
                        content: string,
                        author: AuthorQuery
                    }>
                },
                content: string,
                databaseId: number,
                date: string,
                excerpt: string,
                title: string,
                featuredImageId: string,
                featuredImage: {
                    node: {
                        altText?: string,
                        uri: string,
                        srcSet: string,
                        link: string,
                        title: string,
                        slug: string,
                    }
                }
                isRestricted: boolean,
                isPreview: boolean,
                next: {
                    slug: string
                }
                previous: {
                    slug: string
                }
                relatedPosts: {
                    edges: Array<{
                        node: {
                            id: string,
                            title: string,
                            slug: string,
                            next: {
                                slug: string
                            }
                            previous: {
                                slug: string
                            }
                            featuredImage: {
                                node: {
                                    uri: string,
                                    srcSet: string,
                                    link: string,
                                    title: string,
                                    slug: string,
                                }
                            }
                        }
                    }>
                },
                tags: {
                    edges: Array<{
                        node: {
                            id: string,
                            count: number,
                            description: string,
                            isRestricted: boolean,
                            name: string,
                            slug: string,
                            tagId: number
                        }
                    }>
                }
                slug: string
            }
        }>
    }
}

export interface ImageQuery {
    altText: string,
    caption: string,
    databaseId: number,
    date: string,
    mediaType: string,
    srcSet: string,
    sourceUrl: string,
    title: string,
}

export interface ReviewsQuery {
    reviews: {
        pageInfo: PageInfoQuery,
        nodes: Review[]
    }
}
export interface ReviewBySlugQuery {
    reviewBy: Review
}