import { AppUser } from "../core/interfaces/app-user.interface";
import { Avatar } from "../core/interfaces/media.interface";

export interface userByIDQuery {
    user: {
        id: string,
        avatar: Avatar,
        databaseId: number,
        date?: string,
        description: string,
        firstName: string,
        name: string,
        username: string,
    }
}

export interface usersQuery {
    users: {
        edges: Array<{
            node: {
                id: string,
                avatar: Avatar,
                databaseId: number,
                date: string,
                description: string,
                firstName: string,
                name: string,
                username: string,
            }
        }>
    }
}

export interface RegisterUserResponseQuery {
    registerUser: {
        clientMutationId: any,
        authToken: string,
        refreshToken: string,
        user: {
            id: string,
            avatar: Avatar,
            capabilities: Array<string>,
            email: string,
            databaseId: number,
            description: string,
            firstName: string,
            name: string,
            username: string,
        },
    }

}

export interface LoginUserResponseQuery {
    login: {
        authToken: string,
        refreshToken: string,
        clientMutationId: any,
        user: AppUser,
    }

}