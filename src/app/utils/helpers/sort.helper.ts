export function SortBy(array: any[], key: string) {
    const nArry = [...array];
    nArry.sort((a,b) => {
        const date1 = new Date(a.date).getTime();
        const date2 = new Date(b.date).getTime();
        return date2 < date1 ? 1 : -1;
    });

    return nArry;
}