import * as fromHelpers from './helpers';
import * as fromMappers from './mappers';

export const Utils = {
    Helpers: { ...fromHelpers },
    Mappers: {...fromMappers}
};
