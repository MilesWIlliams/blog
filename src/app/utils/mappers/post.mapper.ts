import { Author } from "src/app/core/interfaces/author.interface";
import { PostComment } from "src/app/core/interfaces/comment.interface";
import { FeaturedImage } from "src/app/core/interfaces/media.interface";
import { Post } from "src/app/core/interfaces/post.interface";
import { RelatedPost } from "src/app/core/interfaces/related-post.interface";
import { postQueryData, relatedPostsQuery } from "src/app/types/post.type";
import { PostsQuery, postsByCategoryQuery, postsByIDQuery } from "src/app/types/types";
import { PostModel } from "../models/post.model";

export class CategoryPostToPosts {
    private posts: Post[] = []
    constructor(data: postsByCategoryQuery) {
        this.posts = data.category.posts.edges.map(
            (p) => {
                let post = new PostModel();

                post.AuthorId = p.node.authorId;
                post.Author = p.node.author.node;
                post.Categories = p.node.categories.edges.map(c => c.node);
                post.commentCount = p.node.commentCount;
                if (p.node.comments) {
                    post.Comments =
                        p.node.comments.nodes.map(c => {
                            const comment: any = c;
                            comment.author = c.author.node;

                            return comment as PostComment
                        });
                }
                post.Content = p.node.content;
                post.Cursor = p.cursor;
                post.DatabaseId = p.node.databaseId;
                post.Date = p.node.date;
                post.Excerpt = p.node.excerpt;
                post.FeaturedImage = p.node.featuredImage?.node;
                post.FeaturedImageId = p.node.featuredImageId;
                post.Id = p.node.id;
                post.IsPreview = p.node.isPreview;
                post.IsRestricted = p.node.isRestricted;
                post.next = p.node.next;
                post.previous = p.node.previous;
                if (p.node.relatedPosts) {
                    post.RelatedPosts = p.node.relatedPosts.edges.map(rp => {
                        const rlPost: any = rp.node;
                        rlPost.next = rp.node.next;
                        rlPost.previous = rp.node.previous;
                        if (rlPost.featuredImage) {
                            rlPost.featuredImage = rp.node.featuredImage.node;
                        }
                        return rlPost as RelatedPost
                    });
                }
                post.Tags = p.node.tags.edges.map(t => t.node);
                post.Slug = p.node.slug;
                post.Title = p.node.title;

                return post;
            }
        )
    }

    get Posts(): Post[] {
        return this.posts;
    }
}

export class MapPosts {
    private posts: Post[] = []
    constructor(data: PostsQuery) {
        this.posts = data.posts.edges.map(
            (p) => {
                let post = new PostModel();

                post.AuthorId = p.node.authorId;
                post.Author = p.node.author.node;
                post.Categories = p.node.categories.edges.map(c => c.node);
                post.commentCount = p.node.commentCount;
                if (p.node.comments) {
                    post.Comments =
                        p.node.comments.nodes.map(c => {
                            const comment: any = c;
                            comment.author = c.author.node;

                            return comment as PostComment
                        });
                }
                post.Content = p.node.content;
                post.Cursor = p.cursor;
                post.DatabaseId = p.node.databaseId;
                post.Date = p.node.date;
                post.Excerpt = p.node.excerpt;
                post.FeaturedImage = p.node.featuredImage?.node;
                post.FeaturedImageId = p.node.featuredImageId;
                post.Id = p.node.id;
                post.IsPreview = p.node.isPreview;
                post.IsRestricted = p.node.isRestricted;

                if (p.node.relatedPosts) {
                    post.RelatedPosts = p.node.relatedPosts.edges.map(rp => {
                        const rlPost: any = rp.node;

                        if (rlPost.featuredImage) {
                            rlPost.featuredImage = rp.node.featuredImage.node;
                        }
                        return rlPost as RelatedPost
                    });
                }
                post.next = p.node.next;
                post.previous = p.node.previous;
                post.Tags = p.node.tags.edges.map(t => t.node);
                post.Slug = p.node.slug;
                post.Title = p.node.title;

                return post;
            }
        )
    }

    get Posts(): Post[] {
        return this.posts;
    }
}

export class MapToPost {
    private post: PostModel = new PostModel();
    constructor(data: any) {

        if (data) {
            if (data.postBy) {
                const unFmtPost = data.postBy as postQueryData;
                this.post = {
                    ...data.postBy
                }
                this.post.author = this.removeNode<Author>(unFmtPost.author) as Author;
                this.post.categories = unFmtPost.categories.edges.map(c => c.node);
                this.post.comments = this.fmtComments(unFmtPost.comments);

                this.post.featuredImage = unFmtPost.featuredImage?.node;
                if (unFmtPost.relatedPosts) { this.post.relatedPosts = MapRelatedPosts(unFmtPost.relatedPosts); }
                this.post.tags = unFmtPost.tags.edges.map((t) => t.node);
            }
            else {
                this.post = {
                    ...data
                };
                this.post.author = this.removeNode<Author>(data.author) as Author;
                this.post.categories = data.categories.edges ? data.categories.edges.map(c => c.node) : data.categories;

                if (data.comments && data.comments.length > 0) { 

                    this.post.comments = this.fmtComments(data.comments); 
                }

                this.post.featuredImage = this.removeNode<FeaturedImage>(data.featuredImage) as FeaturedImage;
                if (data.relatedPosts) { this.post.RelatedPosts = MapRelatedPosts(data.relatedPosts); }
            }
        }
    }

    private removeEdge<T>(data): T | T[] {
        if (data.edge) return data.edge as T;
        if (data.edges) return data.edges as T[];

        return data as T;
    }

    private removeNode<T>(data): T | T[] {
        if(data) {
            if (data.node) return data.node as T;
            if (data.nodes) return data.nodes as T[];

            return data as T;
        }
    }

    private fmtComments(unFmtComments) {
        if (this.removeNode<any[]>(unFmtComments) && this.removeNode<any[]>(unFmtComments).length) {
            unFmtComments = this.removeNode<any[]>(unFmtComments)
            return unFmtComments.map(c => {
                const comment: any = c;
                comment.author = c.author.node;

                return comment as PostComment
            });
        } else {

            unFmtComments = this.removeEdge<any[]>(unFmtComments)

            return unFmtComments.map(c => {
                let comment: any = this.removeNode(c);
                comment.author = this.removeNode(comment.author);

                return comment as PostComment
            });
        }


    }


    get Post(): Post {
        return this.post;
    }
}

export function MapRelatedPosts(relatedPosts: RelatedPost[] | relatedPostsQuery): RelatedPost[] {

    if (relatedPosts) {
        if (instanceOfRelatedPostQuery(relatedPosts)) {
            const posts: RelatedPost[] = relatedPosts.edges.map(p => {
                const post: any = p.node;
                if (post.featuredImage) {
                    post.featuredImage = post.featuredImage.node ? post.featuredImage.node : post.featuredImage;
                }

                return post;
            });

            return posts;
        } else {

            return relatedPosts
        }
    }
}

function instanceOfRelatedPostQuery(data: any): data is relatedPostsQuery {
    return 'edges' in data;
}