import { PageType } from "src/app/core/types/page.types";

export class GenericPage {
    public title: string;
    public slug: string;
    public type: PageType;

    constructor({title, slug, type}) {
        this.title = title;
        this.slug = slug;
        this.type = type
    }
}