import { FeaturedImage } from "src/app/core/interfaces/media.interface";
import { BlogPage } from "src/app/core/interfaces/page.interface";
import { FrontPagePostGroupSection } from "src/app/core/interfaces/post.interface";
import { PageType } from "src/app/core/types/page.types";
import { frontPageDetailsQuery} from "src/app/types/types";
import { GenericPage } from "./generic-page.model";

export class Page extends GenericPage implements BlogPage {
    id: string;
    content: any;
    databaseId: number;
    isFrontPage: boolean;
    featuredImage: FeaturedImage;
    slug: string;
    title: string;
    postsGroups?: FrontPagePostGroupSection;

    constructor(data: frontPageDetailsQuery | BlogPage) {
        super({title: '', slug: '', type: ''});

        if (this.isBlogPage(data)) {
            this.id = data.id;
            this.content = data.content;
            this.databaseId  = data.databaseId;
            this.isFrontPage = data.isFrontPage;
            this.slug = data.slug;
            this.title = data.title;
            this.type = data.type;
            this.featuredImage = data.featuredImage
        } else {
            this.id = data.page.id;
            this.content = data.page.content;
            this.databaseId  = data.page.databaseId;
            this.isFrontPage = data.page.isFrontPage;
            this.featuredImage = data.page.featuredImage?.node;
            this.slug = data.page.slug;
            this.title = data.page.title;
            this.type  =  'page';
            if(this.isFrontPage && data.page.postsGroups) this.postsGroups = data.page.postsGroups;
        }
    }

    private isBlogPage(data: frontPageDetailsQuery | BlogPage): data is BlogPage  {
        if(data) return (<frontPageDetailsQuery>data).page === undefined;
    }
}