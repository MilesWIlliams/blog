import { Author } from "src/app/core/interfaces/author.interface";
import { Category } from "src/app/core/interfaces/category.interface";
import { PostComment } from "src/app/core/interfaces/comment.interface";
import { FeaturedImage } from "src/app/core/interfaces/media.interface";
import { Post } from "src/app/core/interfaces/post.interface";
import { RelatedPost } from "src/app/core/interfaces/related-post.interface";
import { Tag } from "src/app/core/interfaces/tag.interface";
import { PageInfoQuery } from "src/app/types/post.type";

export class PostModel implements Post {
    id: string;
    authorId: string;
    author: Author;
    categories: Category[];
    commentCount: number
    comments?: PostComment[];
    content: string;
    cursor: string;
    databaseId: number;
    date: string;
    excerpt: string;
    featuredImageId: string;
    featuredImage: FeaturedImage;
    isPreview: boolean;
    isRestricted: boolean;
    next: {slug: string};
    previous: {slug: string};
    pageInfo: PageInfoQuery
    relatedPosts: RelatedPost[];
    title: string;
    slug: string;
    tags: Tag[];

    constructor() { }


    public set Id(id: string) {
        this.id = id;
    }

    public set AuthorId(value: string) {
        this.authorId = value;
    }

    public set Author(value: Author) {
        this.author = value;
    }

    public set DatabaseId(value: number) {
        this.databaseId = value;
    }

    public set Date(value: string) {
        this.date = value;
    }

    public set Categories(value: Category[]) {
        this.categories = value;
    }

    public set Comments(value: PostComment[]) {
        this.comments = value;
    }
    public set CommentCount(value: number) {
        this.commentCount = value;
    }

    public set Content(value: string) {
        this.content = value;
    }

    public set Cursor(value: string) {
        this.cursor = value;
    }

    public set Excerpt(value: string) {
        this.excerpt = value;
    }

    public set FeaturedImageId(value: string) {
        this.featuredImageId = value;
    }

    public set FeaturedImage(value: FeaturedImage) {
        this.featuredImage = value;
    }

    public set IsPreview(value: boolean) {
        this.isPreview = value;
    }

    public set IsRestricted(value: boolean) {
        this.isRestricted = value;
    }

    public set RelatedPosts(value: RelatedPost[]) {
        this.relatedPosts = value;
    }

    public set Slug(value: string) {
        this.slug = value;
    }

    public set Tags(value: Tag[]) {
        this.tags = value;
    }

    public set Title(value: string) {
        this.title = value;
    }

}