import { ClipPipe } from "./clip.pipe";
import { SafePipe } from "./safe.pipe";

export const pipes = [
    ClipPipe,
    SafePipe
];